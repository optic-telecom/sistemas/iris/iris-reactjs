import axios from 'axios'
import Config from './../config'

class DatatableService {
	constructor() {
		this.draw = 1
		this.columns = []
		this.order = []
		this.start = 0
		this.length = 10
		this.search = []

		this.get = this.get.bind(this)
		this.formData = this.formData.bind(this)
		this.generator = this.generator.bind(this)
		this.initialize = this.initialize.bind(this)
	}

	config() {
		return {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `${localStorage.getItem('authorization')}`
			}
		}
	}

	initialize = (columns, start = 0, length = 10, order = 0, dir = "desc", sv = "", sr = false) => {
		this.generator(columns)
		this.order.push({
			column: order,
			dir
		})
		this.search = {
			value: sv,
			regex: sr
		}
		this.start = start
		this.length = length
	}

	generator = (columns) => {
		for (let i = 0; i < columns; i++) {
			this.columns.push({
				'data': i,
				'name': "",
				'searchable': true,
				'orderable': true,
				'search': {
					'value': '',
					'regex': false
				}
			})
		}
	}

	formData() {
		const data = new FormData()
		data.append('draw', this.draw)
		data.append('start', this.start)
		data.append('length', this.length)

		// eslint-disable-next-line
		Array.from(this.columns).map((column, index) => {
			data.append(`column[${index}][data]`, column.data)
			data.append(`column[${index}][name]`, column.name)
			data.append(`column[${index}][orderable]`, column.orderable)
			data.append(`column[${index}][searchable]`, column.searchable)
			data.append(`column[${index}][search][value]`, column.search.value)
			data.append(`column[${index}][search][regex]`, column.search.regex)
		})

		// eslint-disable-next-line
		Array.from(this.order).map((order, index) => {
			data.append(`order[${index}][column]`, order.column)
			data.append(`order[${index}][dir]`, order.dir)
		})

		data.append('search[value]', this.search.value)
		data.append('search[regex]', this.search.regex)

		return data
	}

	get = async (endpoint, selector = null) => {
		const data = this.formData()
		return selector
			? await axios.post(`${Config.DATATABLE_URL}/${endpoint}/${selector}/`, data, this.config())
			: await axios.post(`${Config.DATATABLE_URL}/${endpoint}`, data, this.config())
	}
}

export default DatatableService