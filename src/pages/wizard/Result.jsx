import React from 'react';
import { Button } from 'antd';
import { CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons';

function Result(props) {
    return (
        <>
            {props.success ?
                <CheckCircleTwoTone twoToneColor="#52c41a" className='wizard-result-icon' />
                :
                <CloseCircleTwoTone twoToneColor="#ff4d4f" className='wizard-result-icon' />
            }
            <h1 className='form-title'>{props.message}</h1>
            <Button type='primary' onClick={props.goBack} className='center-btn'>Ir al inicio</Button>
        </>
    )
}

export default Result;
