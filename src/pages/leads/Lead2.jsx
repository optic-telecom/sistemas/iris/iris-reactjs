import React, { useState, useEffect } from 'react';
import FullTable from '../../components/datatable/FullTable';
import { Modal, Button } from 'antd';
import { endPointLead } from "../../services/endpoints";
import LeadCreateForm from "../leads/LeadCreateForm";
import HttpRequest from "../../services/HttpRequest";
import "./leads.css";
import { can } from '../../helpers/helpers';
import { lead, leadlet } from '../../config/permissions';
import autoCloseModal from '../../components/modal/AutoCloseModal';
import TypifyComments from '../typify/typifyComments';

function Lead2(props) {
    const [tableRows, setTableRows] = useState([]);
    const [forceReload, setForceReload] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [commentId, setCommentId] = useState(null);

    const endpoints = {
        columns: 'customers/lead_customer/' + props.tableID + '/datatables_struct/',
        rows: 'customers/lead_customer/' + props.tableID + '/datatables/',
        delete: 'customers/lead_customer/',
        download: 'customers/lead_customer/' + props.tableID + '/download_data/',
    }
    const errors = {
        "Error address feasibility": "La dirección no tiene factibilidad",
        "Instance is leadlef": "Ya ha sido convertido a prospecto",
    };
    const convertToProspect = id => {
        if (can(lead.edit) && can(leadlet.create)) {
            HttpRequest.endpoint = "customers/lead_customer/" + id + "/change_to_leadlet/";
            HttpRequest.post().then(res => {
                if (res != {}) {
                    Object.keys(res).forEach(element => {
                        Modal.error({
                            title: "Error",
                            content:
                                "Ha ocurrido un error al convertir este lead. " +
                                errors[element],
                        });
                    });
                } else {
                    autoCloseModal("Se ha convertido a prospecto con éxito")
                    setForceReload(prevState => !prevState);
                }
            });
        }
    };

    const handleRows = res => {
        setTableRows(res.map(row => ({
            ...row,
            custom_to_customer: <Button type="primary" className="form-control" onClick={() => convertToProspect(row.ID)}>Prospecto</Button>,
            commentaries: <Button type="primary" onClick={e => showModal(row.chat)} className="form-control">Comentarios</Button>
        })))
    }

    const showModal = (id) => {
        setCommentId(id)
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
        setCommentId(null)
    };


    return (
        <>
            <FullTable buttons={[]} modalWidth={800} permissions={lead} forceReload={forceReload} modalContent={<LeadCreateForm hideLastName lead={true} endpoint={endPointLead} />} endpoints={endpoints} handleRows={handleRows} tableRows={tableRows} tableName='lead' />
            {
                commentId !== null &&
                <Modal title="Comentarios" visible={isModalVisible} onCancel={handleCancel} footer={null}>
                    <TypifyComments id={commentId} />
                </Modal>
            }
            
        </>
    )
}

export default Lead2;