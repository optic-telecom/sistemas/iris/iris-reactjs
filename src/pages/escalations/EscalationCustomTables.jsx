import React from 'react';
import { escalationTables } from '../../config/permissions';
import TypificationsTable from '../TypificationsTables/TypificationsTables';

function EscalationCustomTables() {
    const endpoints = {
        formEndpoints: ['escalation_ti/escalation_tables/', 'escalation_ti/escalation_tables/definition/', 'escalation_ti/escalation_tables'],
        tableEndpoints: {
            columns: "escalation_ti/escalation_tables/datatables_struct/",
            rows: "escalation_ti/escalation_tables/datatables/",
            delete: "escalation_ti/escalation_tables/",
        }
    }
    return (
        <TypificationsTable endpoints={endpoints} permissions={escalationTables} />
    )
}

export default EscalationCustomTables;