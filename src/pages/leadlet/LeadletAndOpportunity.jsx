import React, { useState } from 'react';
import FullTable from '../../components/datatable/FullTable';
import { Tag, Modal, Button } from 'antd';
import { endPointLeadlet, endPointLeadletTableStruct, endPointLeadletDatatable, endPointOpportunityTableStruct, endPointOpportunityDatatable } from "../../services/endpoints";
import CreateCustomer from "../wizard/CreateCustomer.jsx";
import LeadCreateForm from "../leads/LeadCreateForm";
import ButtonWithModal from "../../components/buttons/ButtonWithModal.jsx";
import ContactForm from './ContactForm';
import { leadlet, client } from '../../config/permissions';
import Wizard from '../wizard/Wizard';

function LeadletAndOpportunity(props) {
    const [tableRows, setTableRows] = useState([]);
    const [forceReload, setForceReload] = useState(false);
    const [prospectId, setProspectId] = useState(null);

    let endpoints;
    if (props.type === 'leadlet') {
        endpoints = {
            columns: endPointLeadletTableStruct,
            rows: endPointLeadletDatatable,
            delete: endPointLeadlet,
            download: "customers/leadlet_customer/download_data/"
        }
    } else {
        endpoints = {
            columns: endPointOpportunityTableStruct,
            rows: endPointOpportunityDatatable,
            delete: endPointLeadlet,
            download: "customers/leadlet_customer/download_data_opportunity/"
        }
    }

    const createCustomer = res => {
        if (res.length > 0) {
            Modal.error({
                title: "Error",
                content:
                    "Ha ocurrido un error al convertir este prospecto a cliente",
            });
        } else {
            Modal.success({
                title: "Éxito",
                content:
                    "Se ha convertido a cliente exitosamente",
            });
        }
    }

    const handleRows = res => {
        setTableRows(res.map(row => ({
            ...row,
            custom_contacts: <Button type='primary' onClick={() => setProspectId(row.ID)}>Contacto</Button>,
            is_customer: row.is_customer == false ?
                <ButtonWithModal
                    name="Convertir"
                    title="Convertir a cliente"
                    onCancel={() => setForceReload(prevState => !prevState)}
                    content={<Wizard LeadletId={row.ID} step={2} searchInfo={true} />}
                    width={1050}
                    permission={[leadlet.edit, client.create]}
                />
                :
                <Tag>{row.is_customer}</Tag>
        })))
    }
    return (
        <>
            <FullTable modalWidth={1000} permissions={leadlet} buttons={[]} forceReload={forceReload} modalContent={<LeadCreateForm endpoint={endPointLeadlet} />} endpoints={endpoints} handleRows={handleRows} tableRows={tableRows} tableName='prospecto' />
            <Modal footer={null} title='Contacto' visible={prospectId} onCancel={() => setProspectId(null)} destroyOnClose>
                <ContactForm id={prospectId} />
            </Modal>
        </>
    )
}

export default LeadletAndOpportunity;