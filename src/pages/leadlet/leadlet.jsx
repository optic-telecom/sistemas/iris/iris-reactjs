import React, { useState, useEffect } from "react";
import { DataTable, ShowFilters } from "../../components/table/ListTable";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import {
	endPointLeadlet,
	endPointLeadletTableStruct,
	endPointLeadletDatatable,
} from "../../services/endpoints";
import "../leads/leads.css";
import LeadletTable from "./leadletTable";

const Leadlet = (props) => {
	return (
		<LeadletTable struct={endPointLeadletTableStruct} datatable={endPointLeadletDatatable} />
	)
};


export default Leadlet;
