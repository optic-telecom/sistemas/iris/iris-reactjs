import React, { useState } from 'react';
import FullTable from '../../components/datatable/FullTable';
import { Modal, Button, Tag } from 'antd';
import { endPointLeadlet, endPointLeadTableStruct, endPointLeadDatatable } from "../../services/endpoints";
import LeadCreateForm from "../leads/LeadCreateForm";
import HttpRequest from "../../services/HttpRequest";
import { can } from '../../helpers/helpers';
import ButtonWithModal from "../../components/buttons/ButtonWithModal.jsx";
import ContactForm from './ContactForm';
import { leadlet, client } from '../../config/permissions';
import Wizard from '../wizard/Wizard';
import TypifyComments from '../typify/typifyComments';

function LeadletTable2(props) {
    const [tableRows, setTableRows] = useState([]);
    const [forceReload, setForceReload] = useState(false);
    const [prospectId, setProspectId] = useState(null);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [commentId, setCommentId] = useState(null);

    const endpoints = {
        columns: 'customers/leadlet_customer/' + props.tableID + '/datatables_struct/',
        rows: 'customers/leadlet_customer/' + props.tableID + '/datatables/',
        delete: 'customers/leadlet_customer/',
        download: 'customers/leadlet_customer/' + props.tableID + '/download_data/',
    }

    const createCustomer = res => {
        if (res.length > 0) {
            Modal.error({
                title: "Error",
                content:
                    "Ha ocurrido un error al convertir este prospecto a cliente",
            });
        } else {
            Modal.success({
                title: "Éxito",
                content:
                    "Se ha convertido a cliente exitosamente",
            });
        }
    }

    const handleRows = res => {
        setTableRows(res.map(row => ({
            ...row,
            custom_contacts: <Button type='primary' onClick={() => setProspectId(row.ID)}>Contacto</Button>,
            is_customer: row.is_customer ?
                <Tag color="green">Ya es cliente</Tag>
                :
                <ButtonWithModal
                    name="Convertir"
                    title="Convertir a cliente"
                    onCancel={() => setForceReload(prevState => !prevState)}
                    content={<Wizard LeadletId={row.ID} step={2} searchInfo={true} />}
                    width={1050}
                    permission={[leadlet.edit, client.create]}
                />,
            commentaries: <Button type="primary" onClick={e => showModal(row.chat)} className="form-control">Comentarios</Button>
        })))
    }

    const showModal = (id) => {
        setCommentId(id)
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
        setCommentId(null)
    };
    return (
        <>
            <FullTable modalWidth={1000} permissions={leadlet} buttons={[]} forceReload={forceReload} modalContent={<LeadCreateForm endpoint={endPointLeadlet} hideLastName />} endpoints={endpoints} handleRows={handleRows} tableRows={tableRows} tableName='prospecto' />
            <Modal footer={null} title='Contacto' visible={prospectId} onCancel={() => setProspectId(null)} destroyOnClose>
                <ContactForm id={prospectId} />
            </Modal>

            {
                commentId !== null &&
                <Modal title="Comentarios" visible={isModalVisible} onCancel={handleCancel} footer={null}>
                    <TypifyComments id={commentId} />
                </Modal>
            }
        </>
    )
}

export default LeadletTable2;