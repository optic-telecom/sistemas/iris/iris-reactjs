import React from 'react';
// import { removeEquipmentTables } from '../../config/permissions';
import TypificationsTable from '../TypificationsTables/TypificationsTables';

function LeadletCustomTables() {
    const endpoints = {
        formEndpoints: ['customers/leadlet_tables/', 'customers/leadlet_tables/definition/', 'customers/leadlet_tables'],
        tableEndpoints: {
            columns: "customers/leadlet_tables/datatables_struct/",
            rows: "customers/leadlet_tables/datatables/",
            delete: "customers/leadlet_tables/",
        }
    }
    return (
        <TypificationsTable
            endpoints={endpoints}
            // permissions={removeEquipmentTables}
            removeCreated={true}
            removeAsigned={true}
        />
    )
}

export default LeadletCustomTables;