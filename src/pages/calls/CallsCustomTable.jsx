import React from 'react';
import TypificationsTable from '../TypificationsTables/TypificationsTables';

function CallsCustomTables() {
    const endpoints = {
        formEndpoints: ['communications/voice_call_tables/', 'communications/voice_call_tables/definition/', 'communications/voice_call_tables'],
        tableEndpoints: {
            columns: "communications/voice_call_tables/datatables_struct/",
            rows: "communications/voice_call_tables/datatables/",
            delete: "communications/voice_call_tables/",
        }
    }
    return (
        <TypificationsTable
            endpoints={endpoints}
            removeCreated={true}
            removeAsigned={true}
        />
    )
}

export default CallsCustomTables;