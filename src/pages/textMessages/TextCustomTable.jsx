import React from 'react';
import TypificationsTable from '../TypificationsTables/TypificationsTables';

function TextCustomTables() {
    const endpoints = {
        formEndpoints: ['communications/text_tables/', 'communications/text_tables/definition/', 'communications/text_tables'],
        tableEndpoints: {
            columns: "communications/text_tables/datatables_struct/",
            rows: "communications/text_tables/datatables/",
            delete: "communications/text_tables/",
        }
    }
    return (
        <TypificationsTable
            endpoints={endpoints}
            removeCreated={true}
            removeAsigned={true}
        />
    )
}

export default TextCustomTables;