import React from 'react';
import TypificationsTable from '../TypificationsTables/TypificationsTables';

function TicketsCustomTables() {
    const endpoints = {
        formEndpoints: ['tech_support/ticket_tables/', 'tech_support/ticket_tables/definition/', 'tech_support/ticket_tables'],
        tableEndpoints: {
            columns: "tech_support/ticket_tables/datatables_struct/",
            rows: "tech_support/ticket_tables/datatables/",
            delete: "tech_support/ticket_tables/",
        }
    }
    return (
        <TypificationsTable
            endpoints={endpoints}
            removeCreated={true}
            removeAsigned={true}
        />
    )
}

export default TicketsCustomTables;