import React from 'react';
import { escalationCategories } from '../../config/permissions';
import Categories from '../category/Categories';

function FinanceEscalationProblems() {
    const endpoints = {
        formEndpoint: 'escalation_finance/problems/',
        tableEndpoints: {
            columns: "escalation_finance/problems/datatables_struct/",
            rows: "escalation_finance/problems/datatables/",
            delete: "escalation_finance/problems/",
        }
    }

    return (
        <Categories endpoints={endpoints} permissions={escalationCategories} />
    )
}

export default FinanceEscalationProblems;