import React, { useState, useEffect } from 'react';
import { DataTable, ShowFilters } from "../../components/table/ListTable";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import ButtonWithModal from "../../components/buttons/ButtonWithModal.jsx";
import { Space, Button, Switch, Modal, Tag, Drawer } from 'antd';
import HttpRequest from "../../services/HttpRequest.js";
import AccordionForm from './AccordionForm';
import Moment from "react-moment";
import CommunicationHistory from './CommunicationHistory';
import './Client.css'

function Client(props) {
    const [tableColumns, setTableColumns] = useState([]);
    const [tableRows, setTableRows] = useState([]);
    const [newClient, setNewClient] = useState(null);
    const [tableLoading, setTableLoading] = useState(true);
    const [reloadRows, setReloadRows] = useState(false);
    const [rowsParams, setRowsParams] = useState(null);
    const [communicationHistoryID, setCommunicationHistoryID] = useState(null);
    const [showDrawer, setShowDrawer] = useState(false);
    const [filters, setFilters] = useState(null);
    const [comparisons, setComparisons] = useState(null);
    const [TOC, setTOC] = useState(null);
    const [queryFilters, setQueryFilters] = useState({});

    const CONSTANTS = [
        "created",
        "custom_change",
        "custom_delete",
        "test",
        "active",
    ];

    const generateExtraButtons = (text, value, id = null) => {
        switch (value) {
            case "created":
                return (
                    <Moment fromNow locale="es">
                        {text}
                    </Moment>
                );
            case "custom_change":
                return (
                    <ButtonWithModal
                        name='Modificar'
                        title='Editar cliente'
                        onCancel={() => setReloadRows(prevState => !prevState)}
                        content={<AccordionForm customerID={id} />}
                    />
                );
            case "custom_delete":
                return (
                    <Button type='primary' className="form-control" danger onClick={() => deleteClient(id)}>Eliminar</Button>
                );
            case "test":
                return (
                    <Switch
                        defaultChecked={text}
                        onChange={'test switch onChange'}
                    />
                );
            case "active":
                return (
                    <Switch
                        defaultChecked={text}
                        onChange={'active switch onChange'}
                    />
                );
            default:
                break;
        }
    };

    const getColumns = () => {
        HttpRequest.endpoint = "customers/customer/datatables_struct/";
        HttpRequest.get().then(res => {
            console.log(res.columns)
            const columnTitles = Object.keys(res.columns);
            const columns = []
            columnTitles.forEach((columnTitle, i) => {
                columns.push({
                    title: columnTitle,
                    dataIndex: res.columns[columnTitle].field,
                    key: res.columns[columnTitle].field,
                    sorter: Object.values(res.columns)[i].sortable,
                    render: CONSTANTS.includes(Object.values(res.columns)[i].field) ?
                        (text, next) => generateExtraButtons(text, Object.values(res.columns)[i].field, next["ID"]) : "",
                })
            })
            setTableColumns(columns);
            setRowsParams(res.defaults);

            let tempTOC = {};
            for (let i = 0; i < Object.keys(res.filters).length; i++) {
                let filterType = Object.values(res.filters)[i].type;
                let arrToCompare = res.type_of_comparisons[filterType]
                let type = [];
                arrToCompare.forEach(element => {
                    type.push({
                        ID: Object.values(res.comparisons[element])[0],
                        name: element,
                    });
                });
                tempTOC[filterType] = type;
            }
            setFilters(res.filters);
            setTOC(tempTOC);
            setComparisons(res.comparisons);
        }).catch(err => console.log(err))
    }
    const getRows = () => {
        if (rowsParams) {
            let data = new FormData();
            data.set(`${Object.keys(rowsParams)[0]}`, `${Object.values(rowsParams)[0]}`);
            data.set(`${Object.keys(rowsParams)[1]}`, `${Object.values(rowsParams)[1]}`);
            data.set(`${Object.keys(rowsParams)[2]}`, `${Object.values(rowsParams)[2]}`);
            data.set(`${Object.keys(rowsParams)[3]}`, `${Object.values(rowsParams)[3]}`);
            data.set(`${Object.keys(rowsParams)[4]}`, JSON.stringify(Object.values(rowsParams)[4]));
            data.set(`${Object.keys(rowsParams)[5]}`, `${Object.values(rowsParams)[5]}`);
            setTableLoading(true);
            HttpRequest.endpoint = "customers/customer/datatables/";
            HttpRequest.post(data).then(res => {
                console.log(res)
                const rows = []
                res.forEach(row => {
                    rows.push({
                        ...row,
                        emails: row.emails.map(email => <Tag key={email}>{email}</Tag>),
                        phones: row.phones.map(phone => <Tag key={phone}>{phone}</Tag>),
                        rut: <div style={{ display: 'inline', color: 'rgb(24, 144, 255)', textDecoration: 'underline', cursor: 'pointer' }} onClick={() => setCommunicationHistoryID(row.ID)}>{row.rut}</div>,
                        key: row.ID
                    })
                })
                setTableRows(rows);
                setTableLoading(false);
            }).catch(err => console.log(err))
        }
    }

    const deleteClient = ID => {
        HttpRequest.endpoint = `customers/customer/`;
        HttpRequest.delete(ID).then(res => {
            if (res.status === 400) {
                Modal.error({
                    title: 'Error',
                    content: 'Ha ocurrido un error eliminando este cliente.'
                })
            } else {
                Modal.success({
                    title: 'Éxito',
                    content: 'Se ha eliminado el cliente con éxito'
                })
                setReloadRows(!reloadRows)
            }
        })
    }
    const updateRowsParams = (keyToUpdate, newValue) => {
        setRowsParams(prevState => ({
            ...prevState,
            [keyToUpdate]: newValue
        }))
    }
    const onSearch = e => {
        e.preventDefault();
        const { name, value } = e.target;
        updateRowsParams(name, value);
    };
    const updateFilters = (keyToUpdate, newValue) => {
        setQueryFilters(prevState => ({
            ...prevState,
            [keyToUpdate]: newValue
        }))
    };
    const setFiltersValue = (key, name, comparison, value) => {
        let arrayFilter = [name, comparison, value];
        let updatedObject = queryFilters;
        Object.assign(updatedObject, { [key]: arrayFilter });
        updateFilters(key, arrayFilter);
        updateRowsParams("filters", Object.values(updatedObject));
    };
    const onChange = e => {
        updateRowsParams("order_field", e.sorter.field);
        e.sorter.order === "ascend"
            ? updateRowsParams("order_type", "asc")
            : updateRowsParams("order_type", "desc");
    };

    const modifyPagination = (action) => {
        if (action === 'back') {
            updateRowsParams('start', rowsParams.start - 10)
        } else {
            updateRowsParams('start', rowsParams.start + 10)
        }
    }
    useEffect(getColumns, [])
    useEffect(getRows, [rowsParams, reloadRows])

    return (
        <>
            <Panel>
                <PanelHeader />
                <PanelBody >
                    <div className="form-group" style={{ textAlign: "right" }}>
                        <Space>
                            <ButtonWithModal
                                name='Crear'
                                title='Crear nuevo cliente'
                                onCancel={() => setReloadRows(prevState => !prevState)}
                                content={<AccordionForm setNewClient={setNewClient} newClient={newClient} />}
                            />
                            <div className="input-group">
                                <input
                                    name="search"
                                    type="text"
                                    className="form-control"
                                    placeholder="Buscar"
                                    onChange={onSearch}
                                />
                                <div className="input-group-append">
                                    <button className="btn btn-outline-secondary" title="Buscar">
                                        <i className="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div className="input-group">
                                <Button className="form-control" onClick={() => setShowDrawer(true)}>Filtros</Button>
                            </div>
                        </Space>
                    </div>
                    <DataTable onChange={onChange} pagination={false} loading={tableLoading} size='default' column={tableColumns} dataSource={tableRows} />
                    <div className='table-pagination'>
                        {rowsParams && rowsParams.start > 9 && <Button onClick={() => modifyPagination('back')}>Atrás</Button>}
                        {tableRows.length > 9 && <Button type='primary' onClick={() => modifyPagination('next')}>Siguiente</Button>}
                    </div>
                    {communicationHistoryID &&
                        <Modal width={850} title='Historial de comunicaciones' visible={communicationHistoryID} onCancel={() => setCommunicationHistoryID(null)} footer={null}>
                            <CommunicationHistory customerID={communicationHistoryID} />
                        </Modal>
                    }
                    <Drawer
                        drawerStyle={{ paddingTop: 48 }}
                        title="Filtros"
                        width={670}
                        onClose={() => setShowDrawer(false)}
                        visible={showDrawer}
                        bodyStyle={{ paddingBottom: 80 }}
                        footer={
                            <div className="form-group" style={{ textAlign: "right" }}>
                                <Button type="danger" onClick={() => setShowDrawer(false)} style={{ marginRight: 8 }}>Cancelar</Button>
                                <Button type="primary" onClick={() => setShowDrawer(false)} style={{ marginRight: 8 }}>Aplicar</Button>
                            </div>
                        }
                    >
                        {filters ?
                            Object.values(filters).map((value, index) => (
                                <ShowFilters
                                    key={index}
                                    name={Object.keys(filters)[index]}
                                    filters={filters}
                                    comparison={comparisons}
                                    typeOfComparison={TOC}
                                    valueFilter={Object.keys(filters)[index]}
                                    function={setFiltersValue}
                                />
                            ))
                            : ""
                        }
                    </Drawer>
                </PanelBody>
            </Panel>
        </>
    )
}

export default Client;