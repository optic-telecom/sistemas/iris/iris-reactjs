import React from 'react';
import { removeEquipmentTables } from '../../config/permissions';
import TypificationsTable from '../TypificationsTables/TypificationsTables';

function RemoveEquipCustomTables() {
    const endpoints = {
        formEndpoints: ['formerCustomers/remove_equipament_tables/', 'formerCustomers/remove_equipament_tables/definition/', 'formerCustomers/remove_equipament_tables'],
        tableEndpoints: {
            columns: "formerCustomers/remove_equipament_tables/datatables_struct/",
            rows: "formerCustomers/remove_equipament_tables/datatables/",
            delete: "formerCustomers/remove_equipament_tables/",
        }
    }
    return (
        <TypificationsTable
            endpoints={endpoints}
            permissions={removeEquipmentTables}
            removeCreated={true}
        />
    )
}

export default RemoveEquipCustomTables;