import React, { useState, useRef, useEffect, } from "react";
import Breadcrumb from "../../components/breadcrumb/Breadcrumb";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import { DataTable, ShowFilters } from "../../components/table/ListTable";
import ButtonWithModal from "../../components/buttons/ButtonWithModal.jsx";
import EventCreateForm from "./EventCreateForm.jsx";
import HttpRequest from "../../services/HttpRequest";
import Moment from "react-moment";
import { connect } from "react-redux";
import { Tag, Modal, Space, Drawer, Button } from "antd";
import {
	endPointCreateEvent,
	endPointTriggerEventDataTable,
	endPointTriggerEventTableStruct,
} from "../../services/endpoints";

const Event = (props) => {
	const [data, setData] = useState([]);
	const [tableColumns, setTableColumns] = useState([]);
	const [tableLoading, setTableLoading] = useState(true);
	const [reloadRows, setReloadRows] = useState(false);
	const [defaultValues, setDefaultValues] = useState(null);
	const [drawer, setDrawer] = useState(false);
	const [filters, setFilters] = useState({});
	const [comparisons, setComparisons] = useState({});
	const [typeOfComparisons, setTypeOfComparisons] = useState({});
	const [queryFilters, setQueryFilters] = useState({});

	const objEndPoint = {
		event: {
			event: endPointCreateEvent,
			struct: endPointTriggerEventTableStruct,
			dataTable: endPointTriggerEventDataTable,
		},
	};

	const rendering = (text, value, id = null) => {
		switch (value) {
			case "created":
				return (
					<Moment fromNow locale="es">
						{text}
					</Moment>
				);
			case "custom_change":
				return (
					<ButtonWithModal
						name="Modificar"
						title="Editar Evento"
						onCancel={() =>
							setReloadRows((prevState) => !prevState)
						}
						content={
							<EventCreateForm id={id} />
						}
					/>
				);
			case "custom_delete":
				return (
					<Button
						type="primary"
						className="form-control"
						danger
						onClick={() => deleteEvent(id)}
					>
						Eliminar
					</Button>
				);

			case "channels":
				let list = [];
				text.map((canal) => {
					switch (canal) {
						case "text":
							list.push(
								<Tag color={"geekblue"} key={canal}>
									Mensaje de texto
								</Tag>
							);
							break;

						case "call":
							list.push(
								<Tag color={"volcano"} key={canal}>
									Llamada
								</Tag>
							);
							break;

						case "whatsapp":
							list.push(
								<Tag color={"green"} key={canal}>
									WhatsApp
								</Tag>
							);
							break;

						case "email":
							list.push(
								<Tag color={"purple"} key={canal}>
									Correo
								</Tag>
							);
							break;

						default:
							break;
					}
				});
				return list;

			default:
				break;
		}
	};

	const special_fields = [
		"created",
		"custom_change",
		"custom_delete",
		"channels"
	];


	const getColumns = () => {
		HttpRequest.endpoint = endPointTriggerEventTableStruct;
		HttpRequest.get()
			.then((res) => {
				console.log(res);
				const columnTitles = Object.keys(res.columns);
				const columns = [];
				columnTitles.forEach((columnTitle, i) => {
					columns.push({
						title: columnTitle,
						dataIndex: res.columns[columnTitle].field,
						key: res.columns[columnTitle].field,
						sorter: Object.values(res.columns)[i].sortable,
						render: special_fields.includes(
							Object.values(res.columns)[i].field
						)
							? (text, next) =>
								rendering(
									text,
									Object.values(res.columns)[i].field,
									next["ID"]
								)
							: "",
					});
				});
				setTableColumns(columns);
				setDefaultValues(res.defaults);
				let tempTOC = {};
				for (let m = 0; m < Object.keys(res.filters).length; m++) {
					let aFilter = Object.values(res.filters)[m].type;
					let arrayTocomp = Object.values(
						res.type_of_comparisons[aFilter]
					);

					let type = [];
					arrayTocomp.forEach((element) => {
						type.push({
							ID: Object.values(res.comparisons[element])[0],
							name: element,
						});
					});
					tempTOC[aFilter] = type;
				}
				setFilters(res.filters); //filters
				setTypeOfComparisons(tempTOC);
				setComparisons(res.comparisons);
			})
			.catch((err) => console.log(err));
	};
	const getRows = () => {
		if (defaultValues) {
			let data = new FormData();
			for (var key in defaultValues) {
				if (key == "filters") {
					data.append(key, JSON.stringify(defaultValues[key]));
					continue;
				}
				data.append(key, defaultValues[key]);
			}
			setTableLoading(true);
			HttpRequest.endpoint = endPointTriggerEventDataTable;
			HttpRequest.post(data).then((res) => {
				const data = [];
				res.forEach((row) => {
					data.push({
						...row,
						key: row.ID,
					});
				});
				if (res.length > 0) {
					setData(data);
				} else {
					if (defaultValues.start != 0) {
						updateDefaults(
							"start",
							Object.values(defaultValues)[2] - 10
						);
					}
					setData(data);
				}
				setTableLoading(false);
			});
		}
	};

	const deleteEvent = (ID) => {
		setReloadRows(false)
		HttpRequest.endpoint = endPointCreateEvent;
		HttpRequest.delete(ID).then((res) => {
			if (res.length > 0) {
				Modal.error({
					title: "Error",
					content: "Ha ocurrido un error eliminando este evento.",
				});
			} else {
				Modal.success({
					title: "Éxito",
					content: "Se ha eliminado el evento con éxito",
					onOk: () => setReloadRows(true)
				});

			}
		});
	};
	const updateDefaults = (keyToUpdate, newValue) => {
		setDefaultValues((prevState) => ({
			...prevState,
			[keyToUpdate]: newValue,
		}));
	};
	const onSearch = (e) => {
		e.preventDefault();
		const { name, value } = e.target;
		updateDefaults(name, value);
	};

	const onChange = (e) => {
		console.log(e.sorter.field);
		updateDefaults("order_field", e.sorter.field);
		updateDefaults(
			"order_type",
			e.sorter.order === "ascend" ? "asc" : "desc"
		);
	};

	const setFiltersValue = (key, name, comparison, value) => {
		let arrayFilter = [name, comparison, value];
		let updatedObject = queryFilters;
		Object.assign(updatedObject, { [key]: arrayFilter });
		setQueryFilters({
			...queryFilters,
			[key]: arrayFilter,
		});
		updateDefaults("filters", Object.values(updatedObject));
		setReloadRows(!reloadRows);
	};

	useEffect(getColumns, []);
	useEffect(getRows, [reloadRows, defaultValues, props.operator]);

	return (
		<>
			<Panel>
				<PanelHeader />
				<PanelBody>
					<div className="form-group" style={{ textAlign: "right" }}>
						<Space>
							<ButtonWithModal
								name="Crear"
								title="Crear nuevo evento"
								onCancel={() =>
									setReloadRows((prevState) => !prevState)
								}
								width="40%"
								content={
									<EventCreateForm
										id={null}
									/>
								}
							/>
							<div className="input-group">
								<input
									name="search"
									type="text"
									className="form-control"
									placeholder="Buscar"
									onChange={onSearch}
								/>
								<div className="input-group-append">
									<button
										className="btn btn-outline-secondary"
										title="Buscar"
									>
										<i className="fas fa-search"></i>
									</button>
								</div>
							</div>
							<div className="input-group">
								<Button
									className="form-control"
									onClick={() => setDrawer(true)}
								>
									Filtros
								</Button>
							</div>
						</Space>
					</div>
					<DataTable
						pagination={false}
						loading={tableLoading}
						size="default"
						column={tableColumns}
						dataSource={data}
						onChange={onChange}
					/>
					<div className="pagination-buttons">
						<Button
							type="primary"
							onClick={() => {
								if (Object.values(defaultValues)[2] !== 0) {
									updateDefaults(
										"start",
										Object.values(defaultValues)[2] - 10
									);
								}
							}}
						>
							Atrás
						</Button>
						<Button
							type="primary"
							style={{ marginLeft: "10px" }}
							onClick={() => {
								updateDefaults(
									"start",
									Object.values(defaultValues)[2] + 10
								);
							}}
						>
							Siguiente
						</Button>
					</div>
					<Drawer
						drawerStyle={{ paddingTop: 48 }}
						title="Filtros"
						width={670}
						onClose={() => setDrawer(false)}
						visible={drawer}
						bodyStyle={{ paddingBottom: 80 }}
						footer={
							<div
								className="form-group"
								style={{
									textAlign: "right",
								}}
							>
								<Button
									type="danger"
									onClick={() => {
										updateDefaults("filters", []);
										setDrawer(false);
									}}
									style={{ marginRight: 8 }}
								>
									Cancelar
								</Button>
								<Button
									type="primary"
									onClick={() => setDrawer(false)}
									style={{ marginRight: 8 }}
								>
									Aplicar
								</Button>
							</div>
						}
					>
						{filters
							? Object.values(filters).map((value, index) => (
								<ShowFilters
									key={index}
									name={Object.keys(filters)[index]}
									filters={filters}
									comparison={comparisons}
									typeOfComparison={typeOfComparisons}
									valueFilter={
										Object.keys(filters)[index]
									}
									function={setFiltersValue}
								/>
							))
							: ""}
					</Drawer>
				</PanelBody>
			</Panel>
		</>
	);
};

function mapStateToProps(state) {
	return {
		operator: state.operator,
	};
}

export default connect(mapStateToProps)(Event);