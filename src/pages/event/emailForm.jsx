import React, { useState, useEffect, useRef } from "react";
import FormComp from "../../components/form/Form.jsx";
import HttpRequest from "../../services/HttpRequest.js";
import { Select, Modal, Button, Form } from "antd";
import { connect } from "react-redux";
import { endPointTemplateEmail, endPointCreateEvent } from "../../services/endpoints.jsx";
import { can } from "../../helpers/helpers.js";

const EmailForm = (props) => {
    const { Option } = Select;
    const [templateList, setTemplateList] = useState();
    const [showForm, setShowForm] = useState(false);
    const formRef = useRef();
    const [template, setTemplate] = useState();
    const [testEmail, setTestEmail] = useState(null);
    const [responseEmail, setResponseEmail] = useState(null);
    const [copyEmail, setCopyEmail] = useState(null);
    const [originEmail, setOriginEmail] = useState(null);

    const createOptions = () => {
        let templatesArray = [];
        HttpRequest.endpoint = endPointTemplateEmail;
        let filters = { operator: props.operator }
        HttpRequest.get(filters)
            .then((res) => {
                for (let index = 0; index < res.length; index++) {
                    templatesArray.push(
                        <Option key={res[index]["ID"]} value={res[index]["ID"]}>
                            {res[index]["name"]}
                        </Option>
                    );
                }
                setTemplateList(templatesArray);
            })
            .then(() => {
                setShowForm(true);
            });
    };

    const addChannel = () => {
        let permission = true;
        if (props.editing) {
            permission = can(props.permissions.edit)
        } else {
            permission = can(props.permissions.create)
        }
        if (permission) {
            let values = formRef.current.values();
            const formData = new FormData();
            formData.append("channel", "email");
            formData.append("email_test", values["testEmail"]);
            formData.append("email_response", values["responseEmail"]);
            values["CopyEmail"] && formData.append("email_copy", values["CopyEmail"]);
            formData.append("email_origin", values["email_origin"]);
            formData.append("template", template);
            HttpRequest.endpoint = endPointCreateEvent + props.id + "/add_channel/";
            HttpRequest.post(formData).then((res) => {
                if (Object.keys(res).length === 0) {
                    Modal.success({
                        title: "Éxito",
                        content: "Canal añadido exitosamente"
                    })
                    props.updateRows()
                } else {
                    Modal.error({
                        title: "Error",
                        content: "Ha ocurrido un error al añadir el canal de comunicación"
                    })
                }
            });
        }
    };

    let formEmail = {
        size: [7, 16],
        title: "Form1",
        items: [{
            custom: false,
            label: "Correo Prueba",
            name: "testEmail",
            type: "input",
            input: "email",
            required: true,
            initialValue: props.id ? testEmail : " ",
        }, {
            custom: false,
            label: "Correo Respuesta",
            name: "responseEmail",
            type: "input",
            input: "email",
            required: true,
            initialValue: props.id ? responseEmail : " ",
        }, {
            custom: false,
            label: "Correo Copia",
            name: "CopyEmail",
            type: "input",
            input: "email",
            required: false,
            initialValue: props.id ? copyEmail : " ",
        },
        {
            custom: false,
            label: "Correo de origen",
            name: "email_origin",
            type: "input",
            input: "email",
            required: true,
            initialValue: props.id ? originEmail : " ",
        },
        ],
        multipart: false,
        submitButton: false,
    };

    const getData = () => {
        if (props.emailEvent) {
            HttpRequest.endpoint = props.emailEvent.split("v1/").pop();
            HttpRequest.get().then((res) => {
                setTemplate(res["template"])
                setTestEmail(res["email_test"]);
                setResponseEmail(res["email_response"]);
                setCopyEmail(res["email_copy"]);
                setOriginEmail(res["email_origin"]);
                createOptions();
            });
        } else {
            createOptions();
        }
    }
    useEffect(() => {
        props.id ? getData() :
            Modal.warning({ title: "Error", content: "Debe crear el evento primero" })
    }, [props.id])

    useEffect(() => {
        if (props.reset) {
            setTemplate(null)
            setOriginEmail(null)
            setCopyEmail(null);
            setResponseEmail(null);
            setTestEmail(null);
            setShowForm(false);
            setTimeout(() => {
                setShowForm(true)
            }, 100)
        }
    }, [props.reset])

    return (
        <div>
            {props.id && showForm &&
                <div>
                    <FormComp ref={formRef} customSubmit={addChannel} info={formEmail} />
                    <Form style={{ width: "100%" }}>
                        <Form.Item required initialValue={template} label="Plantilla" wrapperCol={{ span: 16 }} labelCol={{ span: 6 }}>
                            <Select
                                required
                                placeholder="Seleccione la plantilla"
                                style={{ width: "100%", }}
                                value={template}
                                onChange={(option) => setTemplate(option)} >
                                {templateList}
                            </Select>
                        </Form.Item>
                    </Form>
                    <Button className='center-btn' type="primary" onClick={() => formRef.current.submit()} style={{ marginRight: "10px", }}>
                        Agregar
                    </Button>
                    <Button className='center-btn' style={{ alignSelf: "right" }} type="primary" danger onClick={() => props.permissions ? can(props.permissions.delete) ? props.deleteChannel("email") : null : props.deleteChannel("email")}   >
                        Eliminar
                </Button>
                </div>}
        </div>
    );

}


function mapStateToProps(state) {
    return {
        operator: state.operator,
    };
}
export default connect(mapStateToProps)(EmailForm);