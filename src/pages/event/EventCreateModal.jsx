import React, { useState, useEffect, useRef } from "react";
import { Button, Modal, Dropdown, Menu, Select, Steps } from "antd";
import HttpRequest from "../../services/HttpRequest.js";
import {
	endPointTemplateText,
	endPointTemplateCall,
	endPointTemplateEmail,
	endPointTemplateWhatsapp,
	endPointCreateEvent,
} from "../../services/endpoints.jsx";
import FormComp from "../../components/form/Form.jsx";

/**
 *
 * @param {props} props
 * required:
 * 		visible: bool, sets the modal status from the parent component
 * 		onCancel: function,  change the visible status of the modal so it can be closed from within itself too
 *
 * non required:
 * 		id: int: the event id in case we are going to edit it
 *
 *
 */

const EventCreateModal = (props) => {
	const [type, setType] = useState("Añadir canal de comunicación");
	const [showForm, setShowForm] = useState(false);
	const [showEvent, setShowEvent] = useState(false);
	const [number, setNumber] = useState();
	const [template, setTemplates] = useState();
	const [templateList, setTemplateList] = useState();
	const [testEmail, setTestEmail] = useState(1);
	const [responseEmail, setResponseEmail] = useState(1);
	const [copyEmail, setCopyEmail] = useState(1);
	const [step, setStep] = useState(1);
	const [id, setId] = useState(props.id ?? null);
	const [textEvent, setTextEvent] = useState();
	const [callEvent, setCallEvent] = useState();
	const [whatsappEvent, setWhatsappEvent] = useState();
	const [emailEvent, setEmailEvent] = useState();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const formRef = useRef();
	const formRef2 = useRef();
	const { Option } = Select;
	const { Step } = Steps;

	let info = {
		size: [6, 16],
		title: "Form1",
		items: [
			{
				custom: false,
				label: "Nombre",
				name: "name",
				type: "input",
				input: "string",
				required: true,
				initialValue: props.id ? name : " ",
			},
			{
				custom: false,
				label: "Descripción",
				name: "description",
				type: "textArea",
				input: "string",
				required: true,
				initialValue: props.id ? description : " ",
			},
		],
		multipart: false,
		submitButton: false,
	};

	/**
	 * @function Updates the type of the channel we are currently adding/editing,
	 * changing the displayed form and setting the initial values if necesary.
	 * Also changes the select options dynamically checking the templates list in the db
	 *
	 * @param {string} key option selected in the menu
	 */

	const selectItem = (key) => {
		setType(key["key"]);

		switch (key["key"]) {
			case "correo":
				if (emailEvent) {
					HttpRequest.endpoint = emailEvent.split("v1/").pop();
					HttpRequest.get().then((res) => {
						setTestEmail(res["email_test"]);
						setResponseEmail(res["email_response"]);
						setCopyEmail(res["email_copy"]);
						createOptions(endPointTemplateEmail);
					});
				} else {
					createOptions(endPointTemplateEmail);
				}
				break;

			case "texto":
				if (textEvent) {
					HttpRequest.endpoint = textEvent.split("v1/").pop();
					HttpRequest.get().then((res) => {
						setNumber(res["phone_number_test"]);
						setTemplates(res["template"]);
						createOptions(endPointTemplateText);
					});
				} else {
					createOptions(endPointTemplateText);
				}

				break;

			case "whatsapp":
				if (whatsappEvent) {
					HttpRequest.endpoint = whatsappEvent.split("v1/").pop();
					HttpRequest.get().then((res) => {
						setNumber(res["phone_number_test"]);
						setTemplates(res["template"]);
						createOptions(endPointTemplateWhatsapp);
					});
				} else {
					createOptions(endPointTemplateWhatsapp);
				}

				break;

			case "llamadas":
				if (callEvent) {
					HttpRequest.endpoint = callEvent.split("v1/").pop();
					HttpRequest.get().then((res) => {
						setNumber(res["phone_number_test"]);
						setTemplates(res["template"]);
						createOptions(endPointTemplateCall);
					});
				} else {
					createOptions(endPointTemplateCall);
				}
				break;
			default:
				break;
		}
	};

	/**
	 * @function Retrieves the list of templates and updates the options of the select
	 *
	 * @param {string} endpoint the endpoint to make the request and get the list of templates
	 */

	const createOptions = (endpoint) => {
		let templatesArray = [];
		HttpRequest.endpoint = endpoint;
		HttpRequest.get()
			.then((res) => {
				for (let index = 0; index < res.length; index++) {
					templatesArray.push(
						<Option key={res[index]["ID"]} value={res[index]["ID"]}>
							{res[index]["name"]}
						</Option>
					);
				}
				setTemplateList(templatesArray);
			})
			.then(() => {
				setShowForm(true);
			});
	};

	/**
	 * @function Retrieves the data for the event that we are about to edit. Setting the
	 * initial values and the urls for the request
	 *
	 * @param {string} eventId the Id of the event that we are currently editing
	 */

	async function getData(eventId) {
		let request = "";
		HttpRequest.endpoint = endPointCreateEvent + eventId;
		HttpRequest.get()
			.then((res) => {
				setName(res["name"]);
				setDescription(res["description"]);
				setTextEvent(res["text_event"]);
				setEmailEvent(res["email_event"]);
				setWhatsappEvent(res["whatsapp_event"]);
				setCallEvent(res["call_event"]);
			})
			.then(() => {
				setShowEvent(true);
			});
	}

	/**
	 * @function Called everythime the modal is open/closed. Clears the form fields and
	 * calls the function to retrieve the event data.
	 * Then makes the form visible
	 *
	 * @param {string} eventId the Id of the event that we are currently editing
	 */

	useEffect(() => {
		setName("");
		setDescription("");
		setShowForm(false);
		setShowEvent(false);
		if (props.id && props.visible) {
			getData(props.id);
		} else {
			setShowEvent(true);
		}
	}, [props.visible]);

	function handleSelectChange(option) {
		setTemplates(option["key"]);
	}

	/**
	 * @function Makes the request to the endpoint passing the field values in order to create or update an
	 * event depending on the props.id passed.
	 *
	 */

	const createEvent = () => {
		let values = null;
		const formData = new FormData();
		values = formRef.current.values();
		formData.append("name", values["name"]);
		formData.append("description", values["description"]);
		HttpRequest.endpoint = endPointCreateEvent;

		props.id
			? HttpRequest.patch(props.id, formData).then((res) => {
					setId(props.id);
			  })
			: HttpRequest.post(formData).then((res) => {
					setId(res["ID"]);
					setStep(2);
			  });

		setStep(2);
	};

	/**
	 * @function Makes the request to the endpoint passing the field values depending on the type
	 * in order to create or update a comumnication channel using the event id in the endpoint url.
	 *
	 */

	const addChannel = () => {
		let values = formRef2.current.values();
		const formData = new FormData();
		if (type === "correo") {
			formData.append("channel", "email");
			formData.append("email_test", values["testEmail"]);
			formData.append("email_response", values["responseEmail"]);
			formData.append("email_copy", values["CopyEmail"]);
		} else {
			type === "texto"
				? formData.append("channel", "text")
				: type == "whatsapp"
				? formData.append("channel", "whatsapp")
				: formData.append("channel", "call");

			formData.append("phone_number_test", values["number"]);
		}
		formData.append("template", template);
		HttpRequest.endpoint = endPointCreateEvent + id + "/add_channel/";
		HttpRequest.post(formData).then((res) => {
			console.log(res);
		});

		setStep(1);
		setShowForm(false);
	};

	/**
	 * @function Makes the request to the endpoint passing the field values depending on the type
	 * in order to delete a comumnication channel using the event id in the endpoint url.
	 *
	 */

	const deleteChannel = () => {
		const formData = new FormData();
		switch (type) {
			case "correo":
				formData.append("channel", "email");
				break;
			case "texto":
				formData.append("channel", "text");
				break;
			case "whatsapp":
				formData.append("channel", "whatsapp");
				break;
			case "llamadas":
				formData.append("channel", "call");
				break;
			default:
				break;
		}
		HttpRequest.endpoint = endPointCreateEvent + id + "/remove_channel/";
		HttpRequest.post(formData).then((res) => console.log(res));
	};

	const menu = (
		<Menu onClick={selectItem}>
			<Menu.Item key="correo">
				<i className="far fa-envelope"></i> Correo
			</Menu.Item>
			<Menu.Item key="whatsapp">
				{" "}
				<i className="fa fa-whatsapp"></i> WhatsApp
			</Menu.Item>
			<Menu.Item key="texto">
				<i className="far fa-comments"></i> Texto
			</Menu.Item>
			<Menu.Item key="llamada">
				{" "}
				<i className="fa fa-phone"></i> Llamada de voz
			</Menu.Item>
		</Menu>
	);

	return (
		<Modal
			title={props.id ? "Modificar evento" : "Crear evento"}
			visible={props.visible}
			onCancel={props.onCancel}
			footer={null}
			destroyOnClose={true}
			okText="Listo"
			cancelText="Cerrar"
		>
			<div>
				<Steps current={step}>
					<Step title="Crear Evento" description="" />
					<Step title="Canales de comunicación" description="" />
				</Steps>
				,
				{step === 1 && (
					<div>
						{showEvent && (
							<FormComp
								ref={formRef}
								customSubmit={createEvent}
								info={info}
							/>
						)}

						<Button
							type="primary"
							onClick={() => formRef.current.submit()}
						>
							Guardar y añadir canal
						</Button>
					</div>
				)}
				{step === 2 && (
					<div
						style={{
							marginTop: "10px",
							marginBottom: "10px",
						}}
					>
						<Dropdown overlay={menu} placement="bottomCenter">
							<Button>{type}</Button>
						</Dropdown>
					</div>
				)}
				{showForm ? (
					<div>
						<FormComp
							ref={formRef2}
							customSubmit={addChannel}
							info={{
								size: [6, 16],
								title: "Form1",
								items:
									type === "correo"
										? [
												{
													custom: false,
													label: "Correo Prueba",
													name: "testEmail",
													type: "input",
													input: "email",
													required: true,
													initialValue: props.id
														? testEmail
														: " ",
												},
												{
													custom: false,
													label: "Correo Respuesta",
													name: "responseEmail",
													type: "input",
													input: "email",
													required: true,
													initialValue: props.id
														? responseEmail
														: " ",
												},
												{
													custom: false,
													label: "Correo Copia",
													name: "CopyEmail",
													type: "input",
													input: "email",
													required: true,
													initialValue: props.id
														? copyEmail
														: " ",
												},
												{
													custom: true,
													label: "Plantilla",
													name: "template",
													required: true,
													component: (
														<Select
															placeholder="Seleccione la plantilla"
															labelInValue
															defaultValue={{
																value: template,
															}}
															style={{
																width: "100%",
															}}
															onChange={
																handleSelectChange
															}
														>
															{templateList}
														</Select>
													),
												},
										  ]
										: [
												{
													custom: false,
													label: "Numero Prueba",
													name: "number",
													type: "input",
													input: "string",
													required: true,
													initialValue: props.id
														? number
														: " ",
												},
												{
													custom: true,
													label: "Plantilla",
													name: "template",
													required: true,
													component: (
														<Select
															placeholder="Seleccione la plantilla"
															labelInValue
															defaultValue={{
																value: template,
															}}
															style={{
																width: "100%",
															}}
															onChange={
																handleSelectChange
															}
														>
															{templateList}
														</Select>
													),
												},
										  ],
								multipart: false,
								submitButton: false,
							}}
						/>

						<Button
							type="primary"
							onClick={() => formRef2.current.submit()}
							style={{
								marginRight: "10px",
							}}
						>
							Agregar
						</Button>
						<Button
							style={{ alignSelf: "right" }}
							type="primary"
							danger
							onClick={deleteChannel}
						>
							Eliminar
						</Button>
					</div>
				) : (
					""
				)}
			</div>
		</Modal>
	);
};

export default EventCreateModal;
