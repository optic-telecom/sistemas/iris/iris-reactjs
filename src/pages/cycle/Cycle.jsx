import React, { useState, useRef } from "react";
import Breadcrumb from "../../components/breadcrumb/Breadcrumb";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import SweetAlert from "react-bootstrap-sweetalert";
import ButtonWithModal from "../../components/buttons/ButtonWithModal.jsx";
import CycleList from "./CycleList.jsx";
import CycleCreateForm from "./CycleCreateForm.jsx";
import HttpRequest from "../../services/HttpRequest";
import Moment from "react-moment";
import Button from "../../components/buttons/Button";
import { Switch } from "antd";
import {
	endPointCreateCycle,
	endPointCycleDataTable,
	endPointCycleTableStruct,
} from "../../services/endpoints";

const Cycle = () => {
	const [data, setData] = useState([]);
	const childRef = useRef();

	const errors = {
		"Cycle active":
			"Error. El ciclo ya está activo o ya está ejecutando un test",
		"Datetime error": "Error. La fecha de inicio de este ciclo ya pasó",
	};

	const objEndPoint = {
		cycle: {
			cycle: endPointCreateCycle,
			struct: endPointCycleTableStruct,
			dataTable: endPointCycleDataTable,
		},
	};

	const [showSWAlert, setShowSWAlert] = useState(false);
	const [messageSWA, setMessageSWA] = useState("");

	const hideSWAlert = () => setShowSWAlert(!showSWAlert);

	const fieldRender = [
		"created",
		"custom_change",
		"custom_delete",
		"test",
		"active",
	];

	const redering = (text, value, id = null) => {
		switch (value) {
			case "created":
				return (
					<Moment fromNow locale="es">
						{text}
					</Moment>
				);
			case "custom_change":
				return (
					<ButtonWithModal
						name="Modificar"
						content={<CycleCreateForm data={[id]} />}
						title={"Modificar Ciclo"}
						onCancel={() => {
							childRef.current.getData();
						}}
					></ButtonWithModal>
				);
			case "custom_delete":
				return (
					<Button
						className="form-control"
						color={"danger"}
						onClick={() => {
							handleButtonDelete(id);
						}}
						value={id}
					>
						Eliminar
					</Button>
				);
			case "test":
				return (
					<Switch
						checked={text}
						onChange={(checked) => {
							if (checked) {
								HttpRequest.endpoint =
									endPointCreateCycle + id + "/test/";
								HttpRequest.post(id).then((res) => {
									Object.keys(res).forEach((element) => {
										if (element in errors) {
											setMessageSWA(errors[element]);
											setShowSWAlert(!showSWAlert);
										}
									});
									Object.values(res).forEach((element) => {
										Object.keys(element).forEach(
											(element2) => {
												if (element2 in errors) {
													setMessageSWA(
														errors[element]
													);
													setShowSWAlert(
														!showSWAlert
													);
												}
											}
										);
									});
									childRef.current.getData();
								});
							} else {
								HttpRequest.endpoint =
									endPointCreateCycle + id + "/inactive/";
								HttpRequest.post().then((res) => {
									childRef.current.getData();
								});
							}
						}}
					/>
				);
			case "active":
				return (
					<Switch
						checked={text}
						onChange={(checked) => {
							if (checked) {
								HttpRequest.endpoint =
									endPointCreateCycle + id + "/activate/";
								HttpRequest.post().then((res) => {
									Object.keys(res).forEach((element) => {
										if (element in errors) {
											setMessageSWA(errors[element]);
											setShowSWAlert(!showSWAlert);
										}
									});
									Object.values(res).forEach((element) => {
										Object.keys(element).forEach(
											(element2) => {
												if (element2 in errors) {
													setMessageSWA(
														errors[element]
													);
													setShowSWAlert(
														!showSWAlert
													);
												}
											}
										);
									});
									childRef.current.getData();
								});
							} else {
								HttpRequest.endpoint =
									endPointCreateCycle + id + "/inactive/";
								HttpRequest.post().then((res) => {
									childRef.current.getData();
								});
							}
						}}
					/>
				);
			default:
				break;
		}
	};

	async function handleButtonDelete(id) {
		HttpRequest.endpoint = endPointCreateCycle;
		await HttpRequest.delete(id).then((res) => {
			setMessageSWA("El registro ha sido eliminado de forma exitosa");
			setShowSWAlert(!showSWAlert);
		});
		childRef.current.getData();
	}

	return (
		<React.Fragment>
			<Breadcrumb />
			<div className="row">
				<div className="col-md-12">
					<Panel>
						<PanelHeader></PanelHeader>
						<PanelBody>
							<CycleList
								name={"cycle"}
								endpoints={objEndPoint["cycle"]}
								fieldRender={fieldRender}
								rendering={redering}
								ref={childRef}
								buttons={[
									{
										name: "Crear",
										content: <CycleCreateForm />,
										props: [null],
										title: "Crear ciclo",
									},
								]}
							/>
						</PanelBody>
					</Panel>
				</div>
			</div>
			<SweetAlert
				// success
				show={showSWAlert}
				// title="Buen Trabajo"
				onConfirm={() => hideSWAlert()}
				onCancel={() => hideSWAlert()}
				confirmBtnText={"Aceptar"}
			>
				{messageSWA}
			</SweetAlert>
		</React.Fragment>
	);
};

export default Cycle;
