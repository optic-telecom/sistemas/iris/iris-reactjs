import React, {
    useEffect,
    useState,
    useCallback,
    forwardRef,
    useImperativeHandle,
} from "react";
import HttpRequest from "../../services/HttpRequest";
import Button from "../../components/buttons/Button";
import { connect } from "react-redux";
import { Drawer, Space, Select } from "antd";
import { DataTable, ShowFilters } from "../../components/table/ListTable";
import SweetAlert from "react-bootstrap-sweetalert";
import ButtonWithModal from "../../components/buttons/ButtonWithModal.jsx";
import "moment/locale/es";

const CycleList = forwardRef((props, ref) => {
    const { cycle, struct, dataTable } = props.endpoints;

    const [isInitial, setIsInitial] = useState(true);
    const [initialEndPoint, setInitialEndPoint] = useState(0);
    const [changeData, setChangeData] = useState(false);
    const [isLoadedData, setIsLoadedData] = useState(false);

    const [column, setColumn] = useState([]);
    const [data, setData] = useState([]);
    const [defaultsValues, setDefaultsValues] = useState({});
    const [filters, setFilters] = useState({});
    const [queryFilters, setQueryFilters] = useState({});
    const [drawer, setDrawer] = useState(false);
    const [closeDrawer, setCloseDrawer] = useState(true);

    const [comparisons, setComparisons] = useState({});
    const [typeOfComparisons, setTypeOfComparisons] = useState({});
    const [pagination, setPagination] = useState({ pageSize: 10 });

    const [showModal, setShowModal] = useState(false);
    const [idEvent, setId] = useState();
    const [showSWAlert, setShowSWAlert] = useState(false);
    const [messageSWA, setMessageSWA] = useState("");

    const hideSWAlert = () => {
        setShowSWAlert(!showSWAlert);
        getData();
    };

    async function getColumn() {
        let temp = [];
        let newColumns = [];
        HttpRequest.endpoint = struct;
        await HttpRequest.get().then((res) => {
            temp = Object.keys(res.columns);
            setDefaultsValues(res.defaults);
            for (let index = 0; index < temp.length; index++) {
                newColumns.push({
                    title: temp[index],
                    dataIndex: Object.values(res.columns)[index].field,
                    key: Object.values(res.columns)[index].field,
                    sorter: Object.values(res.columns)[index].sortable,
                    render: props.fieldRender.includes(
                        Object.values(res.columns)[index].field
                    )
                        ? (text, next) =>
                            props.rendering(
                                text,
                                Object.values(res.columns)[index].field,
                                next["ID"]
                            )
                        : "",
                });
            }

            let tempTOC = {};
            for (let m = 0; m < Object.keys(res.filters).length; m++) {
                let aFilter = Object.values(res.filters)[m].type;
                let arrayTocomp = Object.values(
                    res.type_of_comparisons[aFilter]
                );

                let type = [];
                arrayTocomp.forEach((element) => {
                    type.push({
                        ID: Object.values(res.comparisons[element])[0],
                        name: element,
                    });
                });
                tempTOC[aFilter] = type;
            }
            console.log(res)

            setColumn(newColumns); //Columns
            setFilters(res.filters); //filters
            setTypeOfComparisons(tempTOC);
            setComparisons(res.comparisons);
        });
    }

    async function getData() {
        setIsLoadedData(!isLoadedData);

        let data = new FormData();
        data.set(
            `${Object.keys(defaultsValues)[0]}`,
            `${Object.values(defaultsValues)[0]}`
        );

        data.set(
            `${Object.keys(defaultsValues)[1]}`,
            `${Object.values(defaultsValues)[1]}`
        );

        data.set(
            `${Object.keys(defaultsValues)[2]}`,
            `${Object.values(defaultsValues)[2]}`
        );
        data.set(
            `${Object.keys(defaultsValues)[3]}`,
            `${Object.values(defaultsValues)[3]}`
        );
        data.set(
            `${Object.keys(defaultsValues)[4]}`,
            JSON.stringify(Object.values(defaultsValues)[4])
        );
        data.set(
            `${Object.keys(defaultsValues)[5]}`,
            `${Object.values(defaultsValues)[5]}`
        );

        HttpRequest.endpoint = dataTable;

        await HttpRequest.post(data, true, true).then((res) => {
            if (res.length > 0) {
                setData(res);
            } else {
                updateStasteDefaultsValues(
                    "start",
                    Object.values(defaultsValues)[2] - 10
                );
            }
            setIsLoadedData(false);
            console.log(res);
        });
    }

    const onSearch = (e) => {
        e.preventDefault();
        const { name, value } = e.target;
        updateStasteDefaultsValues(name, value);
        setChangeData(true);
    };

    const setFiltersValue = (key, name, comparison, value) => {
        let arrayFilter = [name, comparison, value];
        let updatedObject = queryFilters;
        Object.assign(updatedObject, { [key]: arrayFilter });
        updateStateFilter(key, arrayFilter);
        updateStasteDefaultsValues("filters", Object.values(updatedObject));
        setChangeData(true);
        console.log(queryFilters)
    };

    useImperativeHandle(ref, () => ({
        getData() {
            getData();
        },
    }));

    useEffect(() => {
        const abortController = new AbortController();
        if (isInitial) {
            (async function loadContent() {
                await getColumn();
                setChangeData(true);
                setIsInitial(false);
            })();
        }
        if (changeData) {
            getData();
            setChangeData(false);
        }

        return function cleanup() {
            abortController.abort();
        };
    }, [defaultsValues, initialEndPoint]);

    const updateStateFilter = useCallback(
        (keyToUpdate, newValue) => {
            setQueryFilters({ ...queryFilters, [keyToUpdate]: newValue });
        },
        [queryFilters]
    );

    const updateStasteDefaultsValues = useCallback(
        (keyToUpdate, newValue) => {
            setDefaultsValues({
                ...defaultsValues,
                [keyToUpdate]: newValue,
            });
            console.log(keyToUpdate, newValue);
        },
        [defaultsValues]
    );

    const updateAll = useCallback(() => {
        console.log("updating .....");
    }, [updateStateFilter, updateStasteDefaultsValues]);

    const showDrawer = () => {
        setDrawer(!drawer);
    };

    const onCloseDrawer = () => {
        setDrawer(!closeDrawer);
    };

    const onChange = (e) => {
        updateStasteDefaultsValues("order_field", e.sorter.field);
        e.sorter.order === "ascend"
            ? updateStasteDefaultsValues("order_type", "asc")
            : updateStasteDefaultsValues("order_type", "desc");
        setChangeData(true);
    };

    return (
        <div className="row">
            <div className="col-xs-12">
                <div className="form-group" style={{ textAlign: "right" }}>
                    <Space>
                        {props.buttons.map((element) => {
                            return (
                                <div className="input-group">
                                    <ButtonWithModal
                                        name={element["name"]}
                                        content={React.cloneElement(
                                            element["content"],
                                            {
                                                data: element["props"],
                                            }
                                        )}
                                        title={element["title"]}
                                        onCancel={() => getData()}
                                    />
                                </div>
                            );
                        })}
                        <div className="input-group">
                            <input
                                name="search"
                                type="text"
                                className="form-control"
                                placeholder="Buscar"
                                onChange={onSearch}
                            />
                            <div className="input-group-append">
                                <button
                                    className="btn btn-outline-secondary"
                                    title="Bucar"
                                >
                                    <i className="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <div className="input-group">
                            <Button
                                className="form-control"
                                onClick={() => showDrawer()}
                            >
                                Filtros
							</Button>
                        </div>
                    </Space>
                </div>
                <DataTable
                    column={column}
                    dataSource={data}
                    size={"default"}
                    loading={isLoadedData}
                    pagination={false}
                    onChange={onChange}
                />
                <div
                    style={{
                        width: "100%",
                        justifyContent: "flex-end",
                        display: "flex",
                        marginTop: "20px",
                    }}
                >
                    <Button
                        onClick={() => {
                            if (Object.values(defaultsValues)[2] !== 0) {
                                updateStasteDefaultsValues(
                                    "start",
                                    Object.values(defaultsValues)[2] - 10
                                );
                                setChangeData(true);
                            }
                        }}
                    >
                        Atrás
					</Button>
                    <Button
                        style={{ marginLeft: "10px" }}
                        onClick={() => {
                            updateStasteDefaultsValues(
                                "start",
                                Object.values(defaultsValues)[2] + 10
                            );
                            setChangeData(true);
                        }}
                    >
                        Siguiente
					</Button>
                </div>
                <Drawer
                    drawerStyle={{ paddingTop: 48 }}
                    title="Filtros"
                    width={670}
                    onClose={() => onCloseDrawer()}
                    visible={drawer}
                    bodyStyle={{ paddingBottom: 80 }}
                    footer={
                        <div
                            className="form-group"
                            style={{
                                textAlign: "right",
                            }}
                        >
                            <Button
                                type="danger"
                                onClick={() => onCloseDrawer()}
                                style={{ marginRight: 8 }}
                            >
                                Cancelar
							</Button>
                            <Button
                                type="primary"
                                onClick={() => showDrawer()}
                                style={{ marginRight: 8 }}
                            >
                                Aplicar
							</Button>
                        </div>
                    }
                >
                    {filters
                        ? Object.values(filters).map((value, index) => (
                            <ShowFilters
                                key={index}
                                name={Object.keys(filters)[index]}
                                filters={filters}
                                comparison={comparisons}
                                typeOfComparison={typeOfComparisons}
                                valueFilter={Object.keys(filters)[index]}
                                function={setFiltersValue}
                            />
                        ))
                        : ""}
                </Drawer>
                <SweetAlert
                    success
                    show={showSWAlert}
                    title="Buen Trabajo"
                    onConfirm={() => hideSWAlert()}
                    onCancel={() => hideSWAlert()}
                    confirmBtnText={"Aceptar"}
                >
                    {messageSWA}
                </SweetAlert>
            </div>
        </div>
    );
});

function mapStateToProps(state) {
    return {
        operator: state.operator,
    };
}

export default connect(mapStateToProps)(CycleList);
