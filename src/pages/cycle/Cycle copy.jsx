import React, { useState, useEffect } from "react";
import { Form, Input, Select, InputNumber, Button, Space, DatePicker } from "antd";
import HttpRequest from "../../services/HttpRequest";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons"


function DynamicSelect(props) {

    const [channelList, setChannelList] = useState(
        {
            data: [],
            ready: false
        }
    )

    if (!channelList["ready"]) {

        HttpRequest.endpoint = props.endpoint
        HttpRequest.get().then((res) => {

            setChannelList({
                data: res,
                ready: true
            })

        });

    }

    return (

        <Select>
            {channelList["data"].map((row, index) => <Select.Option key={index} value={row[props.value]}>{row[props.readable_value]}</Select.Option>)}
        </Select>

    )

}

function StaticSelect(props) {

    return (

        <Select>
            {props.data.map((row, index) => <Select.Option key={index} value={row[props.value]}>{row[props.readable_value]}</Select.Option>)}
        </Select>

    )

}

function Cycle() {

    const [componentSize, setComponentSize] = useState("default")
    const onFormLayoutChange = ({ size }) => {
        setComponentSize(size)
    }

    const submit_event = (event) => {

        event.preventDefault()
        debugger

    }

    return (
        <>
            <Form
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 14 }}
                layout="horizontal"
                initialValues={{ size: componentSize }}
                onValuesChange={onFormLayoutChange}
                size={componentSize}
                onFinish={submit_event}
            >
                <Form.Item name="name" label="Nombre del ciclo">
                    <Input />
                </Form.Item>

                <Form.Item name="type_repeats" label="Tipo">
                    <StaticSelect
                        value="value"
                        readable_value="readable_value"
                        data={[
                            {
                                value: "O",
                                readable_value: "Una vez"
                            },
                            {
                                value: "I",
                                readable_value: "Minutos"
                            },
                            {
                                value: "H",
                                readable_value: "Hora"
                            },
                            {
                                value: "D",
                                readable_value: "Diario"
                            },
                            {
                                value: "M",
                                readable_value: "Mensual"
                            },
                            {
                                value: "W",
                                readable_value: "Semanal"
                            },
                            {
                                value: "Q",
                                readable_value: "Trimestral"
                            },
                            {
                                value: "Y",
                                readable_value: "Anual"
                            }
                        ]}
                    />
                </Form.Item>

                <Form.Item name="number_repeats" label="Número de repeticones">
                    <InputNumber />
                </Form.Item>

                <Form.Item name="slack_channel">
                    <DynamicSelect
                        endpoint="slackapp/channel_slack/?fields=name,channel_ID,ID"
                        value="channel_ID"
                        readable_value="name"
                    />
                </Form.Item>

                <Form.List name="massives_communications">
                    {(fields, { add, remove }) => {
                        return (
                            <div>
                                {fields.map(field => (
                                    <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="start">
                                        <Form.Item
                                            {...field}
                                            name={[field.name, 'last']}
                                            fieldKey={[field.fieldKey, 'last']}
                                            rules={[{ required: true, message: 'Fecha de inicio' }]}
                                        >
                                            <DatePicker
                                                format={"YYYY/MM/DD HH:MM:SS"}
                                            />
                                        </Form.Item>
                                        <Form.Item
                                            {...field}
                                            name={[field.name, 'first']}
                                            fieldKey={[field.fieldKey, 'first']}
                                            rules={[{ required: false, message: 'Seleccione masivo' }]}
                                        >
                                            <DynamicSelect
                                                endpoint="communications/massive/?fields=name,ID"
                                                value="ID"
                                                readable_value="name"
                                            />
                                        </Form.Item>

                                        <MinusCircleOutlined
                                            onClick={() => {
                                                remove(field.name);
                                            }}
                                        />
                                    </Space>
                                ))}

                                <Form.Item>
                                    <Button
                                        type="dashed"
                                        onClick={() => {
                                            add();
                                        }}
                                        block
                                    >
                                        <PlusOutlined /> Agrege masivo
                                    </Button>
                                </Form.Item>
                            </div>
                        )
                    }}
                </Form.List>

                <Button htmlType="submit">
                    Crear
                </Button>

            </Form>
        </>
    )

}


export default Cycle