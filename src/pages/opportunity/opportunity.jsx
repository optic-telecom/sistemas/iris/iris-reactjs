import React, { useState, useEffect } from "react";
import {
    endPointOpportunityTableStruct,
    endPointOpportunityDatatable,
} from "../../services/endpoints";
import "../leads/leads.css";
import LeadletTable from "../leadlet/leadletTable";

const Opportunity = (props) => {
    return (
        <LeadletTable button={[]} struct={endPointOpportunityTableStruct} datatable={endPointOpportunityDatatable} />
    )
};


export default Opportunity;