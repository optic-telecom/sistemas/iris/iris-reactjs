import React, { useState, useRef } from "react";
import Breadcrumb from "../../components/breadcrumb/Breadcrumb";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import SweetAlert from "react-bootstrap-sweetalert";
import ButtonWithModal from "../../components/buttons/ButtonWithModal.jsx";
import MassiveCreateForm from "./MassiveCreateForm.jsx";
import MassiveList from "./MassiveList.jsx";
import HttpRequest from "../../services/HttpRequest";
import Moment from "react-moment";
import Button from "../../components/buttons/Button";
import { Tag, Switch } from "antd";
import {
	endPointCreateMassives,
	endPointMassiveDataTable,
	endPointMassiveTableStruct,
} from "../../services/endpoints";

const Massive = () => {
	const [data, setData] = useState([]);
	const childRef = useRef();

	const objEndPoint = {
		massive: {
			massive: endPointCreateMassives,
			struct: endPointMassiveTableStruct,
			dataTable: endPointMassiveDataTable,
		},
	};

	//{showSWAlert} variable used for show or hide the alerts.
	const [showSWAlert, setShowSWAlert] = useState(false);
	const [messageSWA, setMessageSWA] = useState("");

	const hideSWAlert = () => setShowSWAlert(!showSWAlert);

	const fieldRender = [
		"created",
		"custom_change",
		"custom_delete",
		"channels",
		"ID",
	];

	const redering = (text, value, id = null) => {
		switch (value) {
			case "created":
				return (
					<Moment fromNow locale="es">
						{text}
					</Moment>
				);
			case "custom_change":
				return (
					<ButtonWithModal
						name="Modificar"
						content={<MassiveCreateForm id={id} />}
						title={"Modificar Masivo"}
						onCancel={() => {
							childRef.current.getData();
						}}
						width="50%"
					></ButtonWithModal>
				);
			case "custom_delete":
				return (
					<Button
						className="form-control"
						color={"danger"}
						onClick={() => {
							handleButtonDelete(id);
						}}
						value={id}
					>
						Eliminar
					</Button>
				);

			case "channels":
				let list = [];
				text.map((canal) => {
					switch (canal) {
						case "text":
							list.push(
								<Tag color={"geekblue"} key={canal}>
									Mensaje de texto
								</Tag>
							);
							break;

						case "call":
							list.push(
								<Tag color={"volcano"} key={canal}>
									Llamada
								</Tag>
							);
							break;

						case "whatsapp":
							list.push(
								<Tag color={"green"} key={canal}>
									WhatsApp
								</Tag>
							);
							break;

						case "email":
							list.push(
								<Tag color={"purple"} key={canal}>
									Correo
								</Tag>
							);
							break;

						default:
							break;
					}
				});
				return list;
				break;
			case "ID":
				return (
					<Button
						onClick={() => {
							HttpRequest.endpoint =
								endPointCreateMassives + id + "/test/";
							HttpRequest.post(id).then((res) => {
								if (Object.keys(res).length !== 0) {
									setMessageSWA(
										"Ha ocurrido un error al probar el masivo"
									);
									setShowSWAlert(!showSWAlert);
								} else {
									setMessageSWA(
										"La prueba se ha realizado exitosamente"
									);
									setShowSWAlert(!showSWAlert);
								}
							});
						}}
					>
						Probar
					</Button>
				);
				break;
			default:
				break;
		}
	};

	async function handleButtonDelete(id) {
		HttpRequest.endpoint = endPointCreateMassives;
		await HttpRequest.delete(id).then((res) => {
			setMessageSWA("El registro ha sido eliminado de forma exitosa");
			setShowSWAlert(!showSWAlert);
		});
		childRef.current.getData();
	}

	return (
		<React.Fragment>
			<Breadcrumb />
			<div className="row">
				<div className="col-md-12">
					<Panel>
						<PanelHeader></PanelHeader>
						<PanelBody>
							<MassiveList
								name={"massive"}
								endpoints={objEndPoint["massive"]}
								fieldRender={fieldRender}
								rendering={redering}
								ref={childRef}
								buttons={[
									{
										name: "Crear",
										content: <MassiveCreateForm />,
										props: [null],
										title: "Crear masivo",
										width: "50%",
									},
								]}
							/>
						</PanelBody>
					</Panel>
				</div>
			</div>
			<SweetAlert
				show={showSWAlert}
				onConfirm={() => hideSWAlert()}
				onCancel={() => hideSWAlert()}
				confirmBtnText={"Aceptar"}
			>
				{messageSWA}
			</SweetAlert>
		</React.Fragment>
	);
};

export default Massive;
