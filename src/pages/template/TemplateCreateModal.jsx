import React, { useState, useEffect, useRef } from "react";
import { Button, Modal, Input, Dropdown, Menu, Form, Select } from "antd";
import HttpRequest from "../../services/HttpRequest.js";
import {
	endPointTemplateTextTest,
	endPointTemplateCallTest,
	endPointTemplateEmailTest,
	endPointTemplateWhatsappTest,
	endPointWhatsappTags,
	endPointTemplateText,
	endPointTemplateCall,
	endPointTemplateEmail,
	endPointTemplateWhatsapp,
} from "../../services/endpoints.jsx";
import FormComp from "../../components/form/Form.jsx";

/**
 *
 * @param {props} props
 * required:
 * 		visible: bool, sets the modal status from the parent component
 * 		onCancel: function,  change the visible status of the modal so it can be closed from within itself too
 *
 * non required:
 * 		id: int: the event id in case we are going to edit it
 * 		type: the template type that you want to edit (correo, text, whatsapp, llamadadevoz)
 */

const TemplateCreateModal = (props) => {
	const [type, setType] = useState("Tipo de Plantilla");
	const [showForm, setShowForm] = useState(false);
	const [name, setName] = useState("");
	const [content, setContent] = useState("");
	const [subject, setSubject] = useState("");
	const [sender, setSender] = useState("");
	const [reciever, setReciever] = useState("");
	const [number, setNumber] = useState();
	const [selectTags, setSelectTags] = useState([]);
	const [tags, setTags] = useState([]);
	const [form] = Form.useForm();
	const formRefEmail = useRef();
	const formRef = useRef();
	const formRefTestEmail = useRef();
	const formRefTest = useRef();

	/**
	 * @function Updates the type of the template we are currently adding/editing,
	 * changing the displayed form and setting the initial values if necesary.
	 *
	 *
	 * @param {string} key option selected in the menu
	 */

	const selectItem = (key) => {
		setType(key["key"]);
		setShowForm(true);
	};

	const { Option } = Select;

	/**
	 * @function Retrieves the whatsapp tags available from the db
	 *
	 */

	async function getTags() {
		let tagsArray = [];
		HttpRequest.endpoint = endPointWhatsappTags;
		HttpRequest.get().then((res) => {
			res.forEach((element) => {
				tagsArray.push(<Option key={element}>{element}</Option>);
			});
			setSelectTags(tagsArray);
		});
	}

	let PropId = props.id;
	let PropType = props.type;

	/**
	 * @function Retrieves the data for the template that we are about to edit. Setting the
	 * initial values
	 *
	 * @param {string} templateId the Id of the template that we are currently editing
	 * 		  {string} templateType the type of the template that we are currently editing
	 */

	async function getData(templateId, templateType) {
		switch (templateType) {
			case "correo":
				setType("correo");
				HttpRequest.endpoint = endPointTemplateEmail + templateId;
				HttpRequest.get(
					{ fields: "name,subject,template_html" },
					null,
					false
				)
					.then((res) => {
						console.log(res);
						setName(res["name"]);
						setSubject(res["subject"]);
						setContent(res["template_html"]);
					})
					.then(() => setShowForm(true));
				break;
			case "text":
				setType("texto");
				HttpRequest.endpoint = endPointTemplateText + templateId;
				HttpRequest.get({ fields: "name,template" }, null, false)
					.then((res) => {
						setName(res["name"]);
						setContent(res["template"]);
						console.log(res);
					})
					.then(() => setShowForm(true));
				break;
			case "llamadadevoz":
				setType("llamada");
				HttpRequest.endpoint = endPointTemplateCall + templateId;
				HttpRequest.get({ fields: "name,template" }, null, false)
					.then((res) => {
						setName(res["name"]);
						setContent(res["template"]);
						console.log(res);
					})
					.then(() => setShowForm(true));
				break;
			case "whatsapp":
				setType("whatsapp");
				HttpRequest.endpoint = endPointTemplateWhatsapp + templateId;
				HttpRequest.get({ fields: "name,order,template" }, null, false)
					.then((res) => {
						setName(res["name"]);
						setContent(res["template"]);
						setTags(res["order"]);
					})
					.then(() => setShowForm(true));
				break;
			default:
				break;
		}
	}

	useEffect(() => {
		form.resetFields();
		setShowForm(false);
		getTags();
		if (PropId) {
			getData(PropId, PropType);
		}
	}, [props.visible]);

	const handleSubmit = () => {
		type === "correo"
			? formRefEmail.current.submit()
			: formRef.current.submit();
	};

	/**
	 * @function Creates or updates the template with the previously validated field values.
	 * Making a request to the correct endpoint depending on the template type
	 */

	const submitTemplate = () => {
		let values = null;
		values =
			type === "correo"
				? formRefEmail.current.values()
				: formRef.current.values();
		console.log(values);
		const formData = new FormData();
		switch (type) {
			case "correo":
				formData.append("name", name);
				formData.append("template_html", content);
				formData.append("subject", subject);
				HttpRequest.endpoint = endPointTemplateEmail;
				break;

			case "texto":
				formData.append("name", name);
				formData.append("template", content);
				HttpRequest.endpoint = endPointTemplateText;
				break;

			case "whatsapp":
				formData.append("name", name);
				formData.append("template", content);
				formData.append("order", JSON.stringify(tags));
				HttpRequest.endpoint = endPointTemplateWhatsapp;
				break;

			case "llamada":
				formData.append("name", name);
				formData.append("template", content);
				HttpRequest.endpoint = endPointTemplateCall;
				break;

			default:
				break;
		}
		props.id
			? HttpRequest.patch(props.id, formData).then((res) =>
					console.log(res)
			  )
			: HttpRequest.post(formData).then((res) => console.log(res));
	};

	/**
	 * @function Allows to test the template with the previously validated field values.
	 * Making a request to the correct endpoint depending on the template type
	 */

	const testTemplate = () => {
		let values = null;
		values =
			type === "correo"
				? formRefEmail.current.values()
				: formRef.current.values();
		let testValues = null;
		testValues =
			type === "correo"
				? formRefTestEmail.current.values()
				: formRefTest.current.values();
		const formData = new FormData();
		switch (type) {
			case "correo":
				formData.append("template", values["content"]);
				formData.append("subject", values["subject"]);
				formData.append("from_email", testValues["emailSender"]);
				formData.append("to_email", testValues["emailReciever"]);
				HttpRequest.endpoint = endPointTemplateEmailTest;
				break;
			case "texto":
				formData.append("template", values["content"]);
				formData.append("telephone", testValues["number"]);
				HttpRequest.endpoint = endPointTemplateTextTest;
				break;
			case "whatsapp":
				formData.append("template_name", values["name"]);
				formData.append("telephone", testValues["number"]);
				formData.append("params", JSON.stringify(tags));
				HttpRequest.endpoint = endPointTemplateWhatsappTest;
				break;
			case "llamada":
				formData.append("template", values["content"]);
				formData.append("telephone", testValues["number"]);
				HttpRequest.endpoint = endPointTemplateCallTest;
				break;
			default:
				break;
		}

		HttpRequest.post(formData).then((res) => console.log(res));
	};

	function handleSelectChange(value) {
		setTags(value);
	}

	/**
	 * @function Updating the state of the components on change
	 *
	 * @param {event} e the event that holds the new value of the input fields
	 *
	 */

	const checkValidate = (e) => {
		let form = type === "correo" ? formRefEmail : formRef;
		form.current
			.validate()
			.then(() => {
				type === "correo"
					? formRefTestEmail.current.submit()
					: formRefTest.current.submit();
			})
			.catch((error) => console.log(false));
	};

	const forField1 = (
		<FormComp
			customSubmit={submitTemplate}
			ref={formRefEmail}
			info={{
				size: [6, 16],
				title: "Form1",
				items: [
					{
						custom: false,
						label: "Nombre",
						name: "name",
						type: "input",
						input: "string",
						required: true,
						initialValue: props.id ? name : "",
					},
					{
						custom: false,
						label: "Asunto",
						name: "subject",
						type: "input",
						input: "string",
						required: true,
						initialValue: props.id ? subject : "",
					},
					{
						custom: false,
						label: "Contenido",
						name: "content",
						type: "textArea",
						input: "string",
						required: true,
						initialValue: props.id ? content : "",
					},
				],
			}}
		></FormComp>
	);

	let forField2 = (
		<FormComp
			customSubmit={submitTemplate}
			ref={formRef}
			info={{
				size: [6, 16],
				title: "Form1",
				items: [
					{
						custom: false,
						label: "Nombre",
						name: "name",
						type: "input",
						input: "string",
						required: true,
						initialValue: props.id ? name : "",
					},
					{
						custom: false,
						label: "Contenido",
						name: "content",
						type: "textArea",
						input: "string",
						required: true,
						initialValue: props.id ? content : "",
					},
				],
				multipart: false,
				submitButton: false,
			}}
		/>
	);

	let testFormEmail = (
		<FormComp
			customSubmit={testTemplate}
			ref={formRefTestEmail}
			info={{
				size: [6, 16],
				title: "Form1",
				items: [
					{
						custom: false,
						label: "Correo Emisor",
						name: "emailSender",
						type: "input",
						input: "email",
						required: true,
						initialValue: props.id ? sender : "",
					},
					{
						custom: false,
						label: "Correo Receptor",
						name: "emailReciever",
						type: "input",
						input: "email",
						required: true,
						initialValue: props.id ? reciever : "",
					},
				],
				multipart: false,
				submitButton: false,
			}}
		/>
	);

	let testForm = (
		<FormComp
			customSubmit={testTemplate}
			ref={formRefTest}
			info={{
				size: [6, 16],
				title: "Form1",
				items: [
					{
						custom: false,
						label: "Número",
						name: "number",
						type: "input",
						input: "string",
						required: true,
						initialValue: props.id ? number : "",
					},
				],
				multipart: false,
				submitButton: false,
			}}
		/>
	);

	const menu = (
		<Menu onClick={selectItem}>
			<Menu.Item key="correo">
				<i className="far fa-envelope"></i> Correo
			</Menu.Item>
			<Menu.Item key="whatsapp">
				<i className="fab fa-whatsapp"></i> WhatsApp
			</Menu.Item>
			<Menu.Item key="texto">
				<i className="far fa-comments"></i> Texto
			</Menu.Item>
			<Menu.Item key="llamada">
				<i className="fa fa-phone"></i> Llamada de voz
			</Menu.Item>
		</Menu>
	);

	return (
		<Modal
			title={PropId ? "Modificar plantilla" : "Crear plantilla"}
			visible={props.visible}
			onCancel={props.onCancel}
			destroyOnClose={true}
			footer={null}
		>
			<div>
				{PropId ? (
					""
				) : (
					<Dropdown overlay={menu} placement="bottomCenter">
						<Button>{type}</Button>
					</Dropdown>
				)}

				{showForm ? (
					<div
						style={{
							marginTop: "10px",
						}}
					>
						{type === "correo" && forField1}
						{type !== "correo" && forField2}
						{type === "whatsapp" ? (
							<Select
								mode="tags"
								style={{
									width: "60%",
									marginLeft: "25%",
									marginBottom: "15px",
								}}
								placeholder="Por favor seleccione entre las diferentes opciones"
								defaultValue={props.id ? tags : []}
								onChange={handleSelectChange}
							>
								{selectTags}
							</Select>
						) : (
							""
						)}

						<p>Prueba</p>
						{type === "correo" && testFormEmail}
						{type !== "correo" && testForm}

						<Button onClick={checkValidate}>Probar</Button>
						<Button type="primary" onClick={handleSubmit}>
							Crear
						</Button>
					</div>
				) : (
					""
				)}
			</div>
		</Modal>
	);
};

export default TemplateCreateModal;
