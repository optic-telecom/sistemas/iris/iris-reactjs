import React, { useState } from "react";
import "antd/dist/antd.css";
import { Button } from "antd";
import TemplateCreateModal from "./TemplateCreateModal";

const TemplateCreate = (props) => {
	const [showModal, setShowModal] = useState(false);
	const [idTemplate, setId] = useState();
	const [templateType, setTemplateType] = useState("text");

	const openModal = (e) => {
		e.preventDefault();
		setShowModal(true);
	};

	const closeModal = (e) => {
		e.preventDefault();
		setShowModal(false);
	};

	return (
		<div>
			<Button onClick={openModal}>Crear Plantilla</Button>
			<TemplateCreateModal
				type={templateType}
				id={idTemplate}
				visible={showModal}
				onCancel={closeModal}
			/>
		</div>
	);
};

export default TemplateCreate;
