import React, { useState, useEffect, useRef } from "react";
import Breadcrumb from "../../components/breadcrumb/Breadcrumb";
import TabsButton from "../../components/tabs/tabs-button";
import { Panel, PanelBody, PanelHeader } from "../../components/panel/panel";
import SweetAlert from "react-bootstrap-sweetalert";
import TemplateList from "./TemplateList";
import TemplateCreate from "./TemplateCreate";
import TextTemplateForm from "./textTemplateForm.jsx";
import CallTemplateForm from "./callTemplateForm.jsx";
import EmailTemplateForm from "./emailTemplateForm.jsx";
import WhatsappTemplateForm from "./whatsappTemplateForm.jsx";
import ButtonWithModal from "../../components/buttons/ButtonWithModal.jsx";
import Button from "../../components/buttons/Button";
import {
	endPointTemplateText,
	endPointTemplateCall,
	endPointTemplateEmail,
	endPointTemplateWhatsapp,
	endPointTemplateTextTableStruct,
	endPointTemplateCallTableStruct,
	endPointTemplateEmailTableStruct,
	endPointTemplateWhatsappTableStruct,
	endPointTemplateTextDataTable,
	endPointTemplateEmailDataTable,
	endPointTemplateCallDataTable,
	endPointTemplateWhatsappDataTable,
} from "../../services/endpoints";
import Moment from "react-moment";
import HttpRequest from "../../services/HttpRequest";

const Template = (props) => {
	const [data, setData] = useState([]);
	const childRef = useRef();

	const objEndPoint = {
		text: {
			template: endPointTemplateText,
			struct: endPointTemplateTextTableStruct,
			dataTable: endPointTemplateTextDataTable,
		},
		call: {
			template: endPointTemplateCall,
			struct: endPointTemplateCallTableStruct,
			dataTable: endPointTemplateCallDataTable,
		},
		email: {
			template: endPointTemplateEmail,
			struct: endPointTemplateEmailTableStruct,
			dataTable: endPointTemplateEmailDataTable,
		},
		whatsapp: {
			template: endPointTemplateWhatsapp,
			struct: endPointTemplateWhatsappTableStruct,
			dataTable: endPointTemplateWhatsappDataTable,
		},
	};

	const objForms = {
		text: <TextTemplateForm />,
		call: <CallTemplateForm />,
		email: <EmailTemplateForm />,
		whatsapp: <WhatsappTemplateForm />,
	};

	//{display} variable used for show content as pills button selected.
	const [display, setDisplay] = useState(props.type);

	//{record} variable used for carry a configure to the next component.
	const [record, setRecord] = useState("");

	//{showSWAlert} variable used for show or hide the alerts.
	const [showSWAlert, setShowSWAlert] = useState(false);
	const [messageSWA, setMessageSWA] = useState("");

	const hideSWAlert = () => setShowSWAlert(!showSWAlert);

	//Update data Template
	const updateTemplate = (id, name, type, subject, content) => {
		//TODO: Here the endpoint will be set
		setMessageSWA("El registro ha sido editado de forma exitosa");
		setShowSWAlert(!showSWAlert);
		setDisplay("templateList");
	};

	const deleteTemplate = (id) => {
		setMessageSWA("El registro ha sido eliminado de forma exitosa");
		setShowSWAlert(!showSWAlert);
	};

	//This function only callback for changes values from some register.
	const handleChangePillsByChild = (pillsValue, idRecord) => {
		let result = {
			data: data.find((i) => i.id === parseInt(idRecord)),
			showForm: true,
			actionData: updateTemplate,
			isEdit: true,
		};
		setDisplay(pillsValue);
		setRecord(result);
	};

	//This custom array content the fields to will be render in the table
	const fieldRender = ["created", "custom_change", "custom_delete"];

	/**
	 *
	 * @param {String} text This variable is used for datatime
	 * @param {String} value This variable is used to find out which field it render
	 * @param {number} id This variable content {ID} of the record
	 */
	const redering = (text, value, id = null) => {
		switch (value) {
			case "created":
				return (
					<Moment fromNow locale="es">
						{text}
					</Moment>
				);
			case "custom_change":
				return (
					<ButtonWithModal
						name="Modificar"
						content={React.cloneElement(objForms[display], {
							data: [id],
						})}
						title={"Modificar Plantilla"}
						onCancel={() => {
							childRef.current.getData();
						}}
					/>
				);
			case "custom_delete":
				return (
					<Button
						className="form-control"
						color={"danger"}
						onClick={() => {
							handleButtonDelete(id);
						}}
						value={id}
					>
						Eliminar
					</Button>
				);
			default:
				break;
		}
	};

	async function handleButtonDelete(id) {
		HttpRequest.endpoint = objEndPoint[display]["template"];
		await HttpRequest.delete(id).then((res) => {
			console.log("Eliminar", res);

			// setMessageSWA("El registro ha sido eliminado de forma exitosa");
			// setShowSWAlert(!showSWAlert);
		});
		childRef.current.getData();
	}

	const renderTabPanel = (value) => {
		switch (value) {
			case "text":
				return (
					<TemplateList
						name={"text"}
						endpoints={objEndPoint[display]}
						deleteTemplate={deleteTemplate}
						changesPills={handleChangePillsByChild}
						dataSource={data}
						fieldRender={fieldRender}
						rendering={redering}
						ref={childRef}
						buttons={[
							{
								name: "Crear",
								content: <TextTemplateForm />,
								props: [null],
								title: "Crear plantilla",
							},
						]}
					/>
				);
			case "call":
				return (
					<TemplateList
						name={"llamadadevoz"}
						endpoints={objEndPoint[display]}
						deleteTemplate={deleteTemplate}
						changesPills={handleChangePillsByChild}
						dataSource={data}
						fieldRender={fieldRender}
						rendering={redering}
						ref={childRef}
						buttons={[
							{
								name: "Crear",
								content: <CallTemplateForm />,
								props: [null],
								title: "Crear plantilla",
							},
						]}
					/>
				);
			case "email":
				return (
					<TemplateList
						name={"correo"}
						endpoints={objEndPoint[display]}
						deleteTemplate={deleteTemplate}
						changesPills={handleChangePillsByChild}
						dataSource={data}
						fieldRender={fieldRender}
						rendering={redering}
						ref={childRef}
						buttons={[
							{
								name: "Crear",
								content: <EmailTemplateForm />,
								props: [null],
								title: "Crear plantilla",
							},
						]}
					/>
				);
			case "whatsapp":
				return (
					<TemplateList
						name={"whatsapp"}
						endpoints={objEndPoint[display]}
						deleteTemplate={deleteTemplate}
						changesPills={handleChangePillsByChild}
						dataSource={data}
						fieldRender={fieldRender}
						rendering={redering}
						ref={childRef}
						buttons={[
							{
								name: "Crear",
								content: <WhatsappTemplateForm />,
								props: [null],
								title: "Crear plantilla",
							},
						]}
					/>
				);
			default:
				break;
		}
	};

	return (
		<React.Fragment>
			<Breadcrumb />
			<div className="row"></div>
			<div className="row">
				<div className="col-md-12">
					<Panel>
						<PanelHeader></PanelHeader>
						<PanelBody>
							{display === "text" ? renderTabPanel(display) : ""}
							{display === "call" ? renderTabPanel(display) : ""}
							{display === "email" ? renderTabPanel(display) : ""}
							{display === "whatsapp"
								? renderTabPanel(display)
								: ""}
							{display === "templateCreate" ? (
								<TemplateCreate dataSource={record} />
							) : (
								""
							)}
						</PanelBody>
					</Panel>
				</div>
			</div>
			<SweetAlert
				success
				show={showSWAlert}
				title="Buen Trabajo"
				onConfirm={() => hideSWAlert()}
				onCancel={() => hideSWAlert()}
				confirmBtnText={"Aceptar"}
			>
				{messageSWA}
			</SweetAlert>
		</React.Fragment>
	);
};

export default Template;
