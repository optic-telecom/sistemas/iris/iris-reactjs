import React from 'react';
import TypificationsTable from '../TypificationsTables/TypificationsTables';

function EmailCustomTables() {
    const endpoints = {
        formEndpoints: ['communications/email_conversation_tables/', 'communications/email_conversation_tables/definition/', 'communications/email_conversation_tables'],
        tableEndpoints: {
            columns: "communications/email_conversation_tables/datatables_struct/",
            rows: "communications/email_conversation_tables/datatables/",
            delete: "communications/email_conversation_tables/",
        }
    }
    return (
        <TypificationsTable
            endpoints={endpoints}
            // permissions={removeEquipmentTables}
            removeCreated={true}
            removeAsigned={true}
        />
    )
}

export default EmailCustomTables;