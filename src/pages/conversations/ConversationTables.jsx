import React, { useState } from 'react';
import FullTable from '../../components/datatable/FullTable';
function ConversationTables(props) {
    const [tableRows, setTableRows] = useState([]);

    let endpoints = {
        columns: "communications/conversation/" + props.tableID + "/datatables_struct/",
        rows: "communications/conversation/" + props.tableID + "/datatables/",
        delete: `communications/conversation/`,
        download: 'communications/conversation/' + props.tableID + '/download_data/',
    }

    return (
        <>
            <FullTable modalWidth={800} buttons={[]} endpoints={endpoints} tableName='whatsapp_conversations' />
        </>
    )
}

export default ConversationTables;