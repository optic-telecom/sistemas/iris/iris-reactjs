import React, { useState, useEffect } from 'react';
import FullTable from '../../components/datatable/FullTable';
import { Tabs, Tag } from 'antd';
import { connect } from 'react-redux';
import Moment from 'react-moment';
import { can } from '../../helpers/helpers';
import { escalation } from '../../config/permissions';
import Conversation from './Conversation';

function ConversationTab(props) {
    const [tabID, setTabID] = useState(1);
    const [tabList, setTabList] = useState([]);
    const [tableKey, setTableKey] = useState(0);
    const [forceReload, setForceReload] = useState(false);
    const [notifications, setNotifications] = useState([]);
    const infoNotification = JSON.parse(localStorage.getItem('idNotification'))

    useEffect(() => {
        if (props.notifications != notifications) {
            setNotifications(props.notifications)
            setForceReload(prevState => !prevState);
        }

    }, [props.notifications])

    useEffect(() => {
        if (infoNotification) {
            createTab(infoNotification, infoNotification)
        }
    }, [infoNotification])


    let endpoints = {
        columns: props.endpoints.columns + props.tableID + "/datatables_struct/",
        rows: props.endpoints.rows + props.tableID + "/datatables/",
        download: props.endpoints.download + props.tableID + '/download_data/',
    }


    const customRendering = (text, value, id) => {
        switch (value) {
            case "created":
                return <Moment fromNow locale="es">{text}</Moment>;
            case "custom_chat":
                return <div style={{ display: 'inline', color: 'rgb(24, 144, 255)', textDecoration: 'underline', cursor: 'pointer' }} onClick={() => createTab(id, text)}>{id}</div>
            case "updated":
                return <Moment fromNow locale="es">{text}</Moment>;
            case "services":
                return text.map(serv => <Tag key={id}>{serv}</Tag>)
            default:
                break;
        }
    }
    const createTab = (escID, service) => {
        if (can(escalation.view)) {
            setTabID(prevTabID => {
                setTabList(prevTabList => {
                    let isNew = true;
                    if (prevTabID) {
                        prevTabList.forEach(tab => {
                            if (tab.props.tab === service) {
                                isNew = false;
                            }
                        })
                    }
                    if (isNew) {
                        return prevTabList.concat([
                            <Tabs.TabPane tab={service} key={prevTabID} closeTab={deleteTab}>
                                <Conversation type={props.type} messagesEndpoint={props.messagesEndpoint} id={escID} />
                            </Tabs.TabPane>
                        ])
                    } else {
                        return prevTabList
                    }
                })
                return prevTabID + 1;
            })
        }
    }
    const deleteTab = tabID => {
        setTabList(prevTabList => prevTabList.filter(tab => Number(tab.key) !== tabID))
    }

    useEffect(() => {
        setTabList([])
    }, [props.operator])

    return (
        <>

            <Tabs className='typify-tab' type='card' size='small' onChange={e => { if (e.includes('table')) { setTableKey(tableKey + 1) } }}>
                <Tabs.TabPane tab='Tabla' key='table'>
                    {props.tableID && <FullTable forceReload={forceReload} customRendering={{ renderFunction: customRendering, keys: ['ID', 'updated', 'service', 'custom_chat'] }} key={tableKey} modalWidth={800} buttons={[]} endpoints={endpoints} tableName='whatsapp_conversations' />}
                </Tabs.TabPane>
                {tabList}
            </Tabs>

        </>
    )
}

function mapStateToProps(state) {
    return {
        operator: state.operator,
        notifications: state.notifications
    };
}

export default connect(mapStateToProps)(ConversationTab);