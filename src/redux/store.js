import {createStore, combineReducers} from 'redux'
import reducers from './reducers'

const combinedReducers = combineReducers({...reducers})

export default createStore(combinedReducers)