import React from 'react'

const StatusIcon = ({status=false}) => (
	<i className={`ion ion-md-globe fa-2x fa-fw ${status ? 'text-success' : 'text-danger'}`}></i>
)

export default StatusIcon