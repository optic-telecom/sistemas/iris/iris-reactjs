import React from 'react'
import Icon from './Icon'
import styled from 'styled-components'

const SpinnerUI = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
	& > i {
		margin-right: 1rem;
	}
`

const Spinner = props => (
	<SpinnerUI>
		<Icon icon="spinner" animation="pulse" size={2} />
		Cargando datos
	</SpinnerUI>
);

export default Spinner