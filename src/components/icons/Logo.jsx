import React from 'react'
import styled from 'styled-components'

import LogoPNG from './../../assets/images/logo.png'

const LogoUI = styled.img`
	position: relative;
	bottom: .3rem;
	&.size-1 {
		width: 50px;
	}
`

const Logo = ({size = 1}) => (
	<LogoUI src={LogoPNG} className={`size-${size}`}/>
);

export default Logo