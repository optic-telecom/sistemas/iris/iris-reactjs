import React from 'react'

const Icon = ({fas=true, far=false, icon=null, animation=null, size=1, className, onClick}) => (
	<i className={`${fas && "fas"} ${far && "far"} ${icon && `fa-${icon}`} ${animation && `fa-${animation}`} ${size && `fa-${size}x`} ${className}`} onClick={onClick}></i>
);

export default Icon