import React from 'react'
import PropTypes from 'prop-types'

const TabsContent = ({ id, name, filters, table }) => (
	<div className="tabs-content">
		{name}
	</div>
);

TabsContent.defaultProps = {
	filters: null,
	table: null
}

TabsContent.propTypes = {
	id: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	filters: PropTypes.object,
	table: PropTypes.object
}

export default TabsContent