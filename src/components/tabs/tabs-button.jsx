import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const TabsButtonUI = styled.button`
	border-radius: 5px;
	padding: .5rem .75rem;
	margin-bottom: .5rem;
	margin-right: .5rem;
	border: 0;
	outline: none;
	background-color: var(--inverse);
	color: white;
	&.active {
		background-color: var(--primary-color);
	}
	.tabs-close {
		opacity: .5;
		margin-left: .5rem;
		&:hover {
			opacity: 1;
		}
	}
`

const TabsButton = ({ children, isActive, onClick, onClose, value }) => (
	<TabsButtonUI onClick={onClick} className={isActive ? 'active' : ''} value={value} >
		{children}
		{onClose && <span className="tabs-close" onClick={onClose}>x</span>}
	</TabsButtonUI>
);

TabsButton.defaultProps = {
	onClose: null,
	isActive: false
}

TabsButton.propTypes = {
	isActive: PropTypes.bool,
	children: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired,
	onClose: PropTypes.oneOfType([PropTypes.func, PropTypes.instanceOf(null)]),
}

export default TabsButton