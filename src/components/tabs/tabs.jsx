import React, { Component } from "react";
import PropTypes from "prop-types";
import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import classnames from "classnames";

class Tabs extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activeTab: 1,
		};
	}

	toggleTab(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab,
			});
		}
	}

	render() {
		return (
			<React.Fragment>
				<Nav tabs>
					<NavItem>
						<NavLink
							className={classnames({ active: this.state.activeTab === "1" })}
							onClick={() => {
								this.toggleTab("1");
							}}
						>
							<span className="d-sm-none">{/* {this.props.title} */} hola</span>
							<span className="d-sm-block d-none">
								{/* {this.props.title} */}hola
							</span>
						</NavLink>
					</NavItem>
				</Nav>
				<TabContent activeTab={this.state.activeTab}>
					<TabPane tabId="">
						{/* {this.props.component ? this.props.component : ""} */}
					</TabPane>
				</TabContent>
			</React.Fragment>
		);
	}
}

export default Tabs;
