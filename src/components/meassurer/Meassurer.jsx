import React, {useEffect, useState, useRef} from 'react'
import PropType from 'prop-types'
import {useInterval} from './../../hooks/hooks'

const Meassurer = ({progress, width, color, message, className}) => {
	const meassurerRef = useRef()
	const tooltipRef = useRef()
	const [_progress, increment] = useState(0)
	const [coords, mouseCoords] = useState({x: 0, y: 0})

	useEffect(() => {
		meassurerRef.current.style.setProperty('--width', `${width}px`)
		meassurerRef.current.style.setProperty('--color', `var(--secondary-color)`)
	})

	useInterval(() => {
		if(_progress <= progress) {
			meassurerRef.current.style.setProperty('--progress', _progress)
			increment(_progress+1)
		}
	}, 30)

	const activeTooltip = (e) => {
		const height = tooltipRef.current.clientHeight * 4
		const width = tooltipRef.current.clientWidth * 0.5
		mouseCoords({x: e.screenX - width, y: e.screenY - height})
	}

	const disableTooltip = () => mouseCoords({x: 0, y: 0})

	return (
		<div className={`meassurer ${className}`} ref={meassurerRef} onMouseOver={activeTooltip} onMouseLeave={disableTooltip}>
			<div className="overflow">
				<div className="bar"></div>
			</div>
			<div className="dial"></div>
			{
				(message !== "") &&
				<div
					className="_tooltip"
					style={{top: `${coords.y}px`, left: `${coords.x}px`}}
					ref={tooltipRef}>{message}</div>
			}
		</div>
	)
}

Meassurer.propTypes = {
	progress: PropType.number,
	width: PropType.number,
	color: PropType.string,
	message: PropType.string
}

Meassurer.defaultProps = {
	progress: 0,
	width: 200,
	color: '90ca4b',
	message: ""
}

export default Meassurer