import React from 'react'
import PropType from 'prop-types'

const FindingMessage = ({text}) => (
	<p className="finding-message">{text}</p>
);

FindingMessage.propTypes = {
	text: PropType.string
}

FindingMessage.defaultProps = {
	text: "Buscando"
}

export default FindingMessage