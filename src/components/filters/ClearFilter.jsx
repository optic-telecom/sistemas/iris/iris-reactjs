import React from 'react'
import styled from 'styled-components'
import Button from '../buttons/Button';

const ButtonUI = styled(Button)`
	margin-right: 1rem;
`

const ClearFilter = ({onClick}) => (
	<ButtonUI onClick={onClick}>Reset</ButtonUI>
);

export default ClearFilter