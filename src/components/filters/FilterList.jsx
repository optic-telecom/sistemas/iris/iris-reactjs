import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const SelectUI = styled.select`
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: .45rem;
	margin-right: 1rem;
	&.full {
		width: 100%;
	}
`

const FilterList = ({name, options, onChange, full, className}) => {
	const handleOnChange = event => {
		event.preventDefault()

		const response = {
			value: event.target.value !== "" ? event.target.value : "",
			label: event.target.value !== "" ? event.target.options[event.target.selectedIndex].textContent : ""
		}

		return onChange(response)
	}

	return (
		<SelectUI onChange={handleOnChange} className={`${className} ${full ? 'full' : ''}`}>
			<option value="">{name}</option>
			{
				options ? options.map((item, index) => <option key={index} value={item.value}>{item.label}</option>) : null
			}
		</SelectUI>
	);
}

FilterList.propTypes = {
	className: PropTypes.string,
	full: PropTypes.bool
}

FilterList.defaultProps = {
	className: "",
	full: false
}

export default FilterList