import React, { useState } from "react";
import { connect } from "react-redux";

import ACTIONS from "./../../redux/creator";

import { Panel, PanelHeader, PanelBody } from "../panel/panel";
import Icon from "../icons/Icon";
import EmptyContent from "./../content/empty";
import PanelRightHeader from "./panel-right-header";
import { GeneralTab } from "./sidebar-content/general";

const PanelRight = (props) => {
	const [section, changeSection] = useState("general");

	const toggle = () => {
		const sidebar = props.sidebar;
		const newState = {
			...props.sidebar,
			active: !sidebar.active,
			show: !sidebar.show,
		};
		props.toggleSidebar(newState);
	};

	const handleSection = (event) => changeSection(event.target.value);

	return (
		<div
			className={`panel-right ${
				props.sidebar.show
					? "active show"
					: !props.sidebar.show
					? "active disable"
					: ""
			}`}
		>
			<div className="panel-right-toggle" onClick={toggle}>
				<Icon icon="cog" />
			</div>
			<Panel>
				<PanelBody>
					{!props.sidebar || !props.sidebar.child ? (
						<EmptyContent />
					) : (
						<>
							<PanelRightHeader handleSection={handleSection} />
							<hr />
							{section === "general" && <GeneralTab />}
						</>
					)}
				</PanelBody>
			</Panel>
		</div>
	);
};

const mapStateToProps = (state) => ({
	sidebar: state.sidebar,
});

const mapDispatchToProps = (dispatch) => ({
	toggleSidebar(payload) {
		dispatch(ACTIONS.toggleSidebar(payload));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(PanelRight);
