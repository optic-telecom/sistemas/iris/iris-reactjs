import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const CardUI = styled.div`
	border: 1px solid #ddd;
	border-radius: 5px;
	border-bottom: 2px solid #ddd;
	padding: 1rem;
	padding-bottom: .2rem;
	margin-bottom: 1rem;
`

const Card = ({children, className}) => (
	<CardUI className={className}>
		{children}
	</CardUI>
);

Card.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string
}

Card.defaultProps = {
	children: "",
	className: ""
}

export default Card