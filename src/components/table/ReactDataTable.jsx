import React, { useState, useEffect } from "react";
import styled from "styled-components";
import DataTable from "react-data-table-component";

// Components
import Button from "./../../components/buttons/Button";
import { device } from "./../../helpers/helpers";

const TableOptions = styled.div`
	--margin: 0.5rem;
	display: flex;
	flex-flow: row nowrap;
	margin-bottom: calc(var(--margin) * 2);
	width: 100%;
	justify-content: space-between;
	align-items: center;
	.filter {
		display: flex;
		flex-flow: row nowrap;
		justify-content: flex-end;
		width: 100%;
		align-items: center;
	}
	.input-group {
		display: flex;
		padding: var(--margin);
		flex-flow: row nowrap;
		width: 100%;
		border-radius: 4px;
		border: 1px solid #ddd;
		input[type="text"],
		input[type="number"] {
			width: 90%;
			border: 0;
			background: none;
			outline: none;
			padding: 0;
		}
		.fas {
			margin-right: var(--margin);
			margin-left: var(--margin);
		}
	}
	.filter-toggle {
		display: none;
		margin-left: 1rem;
		font-size: 1rem;
		button:nth-child(2) {
			margin-left: 1rem;
		}
	}

	@media ${device.desktop} {
		.filter {
			width: 50%;
		}
		.filter-toggle {
			display: block;
		}
		.input-group {
			max-width: 250px;
		}
	}
`;

const TableColumnFilter = styled.input`
	width: 100%;
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 0.25rem;
	outline: none;
`;

const TableColumnCheckbox = styled.input`
	display: table;
	margin: auto;
`;

const ReactDataTable = ({
	name,
	columns,
	data,
	pagination,
	paginationServer,
	paginationTotalRows,
	onChangeRowsPerPage,
	onChangePage,
	buttons,
	filterable,
	filters,
}) => {
	const [advanced, toggleAdvanced] = useState(false);
	const [showDisplayOption, toggleDisplayOption] = useState(false);
	const [dataTable, filterDataTable] = useState(data);
	const [advancedData, setAdvancedData] = useState([]);
	const [displayOptionRow, setDisplayOptionRow] = useState([]);
	const [visibleColumns, setVisibleColumns] = useState(columns);
	const [preferences, setPreferences] = useState(
		JSON.parse(localStorage.getItem("preferences"))
	);
	const [totalRows, setTotalRows] = useState(paginationTotalRows);

	const filter = (e) => {
		const result = [];
		const keys = Object.keys(data[0]);
		data.map((_row) => {
			let skip = false;
			return keys.map((key) => {
				if (
					`${_row[key]}`
						.toLowerCase()
						.search(e.target.value.toLowerCase()) !== -1 &&
					!skip
				) {
					skip = true;
					result.push(_row);
				}
				return true;
			});
		});

		return advanced
			? setAdvancedData(setInputsIntoTable(result))
			: filterDataTable(result);
	};

	// eslint-disable-next-line
	const filterByRow = (e, key) => {
		const result = [];
		// eslint-disable-next-line
		data.map((_row) => {
			let skip = false;

			if (
				`${_row[key]}`
					.toLowerCase()
					.search(e.target.value.toLowerCase()) !== -1 &&
				!skip
			) {
				skip = true;
				result.push(_row);
			}
		});

		return setAdvancedData(setInputsIntoTable(result));
	};

	const handleAdvanceState = () => {
		toggleAdvanced(!advanced);
	};

	// eslint-disable-next-line
	const setColumnsVisibility = () => {
		if (preferences && preferences[name])
			setVisibleColumns(
				columns.filter(
					(column) => preferences[name][column.selector] !== false
				)
			);
	};

	const handlePreferences = (e, selector) => {
		const currentPreferences = JSON.parse(
			localStorage.getItem("preferences")
		);
		const _section = preferences ? currentPreferences[name] : {};

		const newPreferences = {
			...currentPreferences,
			[name]: {
				..._section,
				[selector]: e.target.checked,
			},
		};

		localStorage.setItem("preferences", JSON.stringify(newPreferences));
		setPreferences(newPreferences);
		setVisibleColumns(
			columns.filter(
				(column) => newPreferences[name][column.selector] !== false
			)
		);
		setDisplayOptionRow(setDisplayColumnOption);
	};

	// eslint-disable-next-line
	const setInputsIntoTable = (data) => {
		let row = {};

		// eslint-disable-next-line
		columns.map((column, key) => {
			row = {
				...row,
				[column.selector]: (
					<TableColumnFilter
						key={key}
						onChange={(e) => filterByRow(e, [column.selector])}
					/>
				),
			};
		});

		return data ? [row, ...data] : [row, ...dataTable];
	};

	// eslint-disable-next-line
	const setDisplayColumnOption = () => {
		let row = {};
		const currentPreferences = JSON.parse(
			localStorage.getItem("preferences")
		);

		// eslint-disable-next-line
		columns.map((column, key) => {
			row = {
				...row,
				[column.selector]: (
					<>
						{currentPreferences[name] &&
						currentPreferences[name][column.selector] === false ? (
							<TableColumnCheckbox
								key={key}
								type="checkbox"
								defaultChecked={false}
								onChange={(e) =>
									handlePreferences(e, [column.selector])
								}
							/>
						) : (
							<TableColumnCheckbox
								key={key}
								type="checkbox"
								defaultChecked={true}
								onChange={(e) =>
									handlePreferences(e, [column.selector])
								}
							/>
						)}
					</>
				),
			};
		});

		return [row];
	};

	const handleTotalRows = (e) => {
		const currentPreferences = JSON.parse(
			localStorage.getItem("preferences")
		);
		const _section = preferences ? currentPreferences[name] : {};

		const newPreferences = {
			...currentPreferences,
			[name]: {
				..._section,
				rows: e.target.value,
			},
		};

		setTotalRows(e.target.value);
		setPreferences(newPreferences);
		localStorage.setItem("preferences", JSON.stringify(newPreferences));
	};

	// eslint-disable-next-line
	useEffect(() => {
		setAdvancedData(setInputsIntoTable());
		setDisplayOptionRow(setDisplayColumnOption());
		setColumnsVisibility();

		if (preferences && preferences[name] && preferences[name].rows)
			setTotalRows(preferences[name].rows);
		// eslint-disable-next-line
	}, []);

	return (
		<>
			<TableOptions>
				{buttons && buttons.length > 0 && (
					<div className="buttons">{buttons.map((btn) => btn)}</div>
				)}
				<div className="filter">
					{filterable && advanced ? filters : null}
					<div className="input-group">
						<div className="input-group__icon">
							<span className="fas fa-search"></span>
						</div>
						<input type="text" onChange={filter} />
					</div>
					<div className="filter-toggle">
						<Button color="default" onClick={handleAdvanceState}>
							<span className="fas fa-sliders-h clickable"></span>
						</Button>
						<Button
							color="default"
							onClick={() =>
								toggleDisplayOption(!showDisplayOption)
							}
						>
							<span className="fas fa-palette clickable"></span>
						</Button>
					</div>
				</div>
			</TableOptions>
			{showDisplayOption ? (
				<>
					<div className="row">
						<div className="col-sm-2">
							<input
								type="number"
								min="1"
								defaultValue={totalRows}
								className="form-control"
								placeholder="Cantidad de registros"
								onChange={handleTotalRows}
							/>
						</div>
						<div className="col-sm-10"></div>
					</div>
					<DataTable
						className={`is_display_options`}
						columns={columns}
						data={displayOptionRow}
					/>
				</>
			) : (
				<DataTable
					className={`${advanced ? "is_advanced" : ""}`}
					columns={visibleColumns}
					data={
						showDisplayOption
							? displayOptionRow
							: advanced
							? advancedData
							: dataTable
					}
					pagination={pagination}
					paginationServer={paginationServer}
					paginationTotalRows={totalRows}
					onChangeRowsPerPage={onChangeRowsPerPage}
					onChangePage={onChangePage}
				/>
			)}
		</>
	);
};

export default ReactDataTable;
