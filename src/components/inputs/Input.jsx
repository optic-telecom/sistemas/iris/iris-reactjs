import React from 'react'
import PropTypes from 'prop-types'

const Input = ({className, name, onClick, onChange, masked, autoComplete, ...rest}) => (
	<input
		name={name}
		className={`input-control ${masked ? "input-control--masked" : ""} ${className}`}
		onClick={onClick}
		onChange={onChange}
		autoComplete={autoComplete}
		{...rest} />
);

Input.propTypes = {
	className: PropTypes.string,
	onClick: PropTypes.func,
	onChange: PropTypes.func,
	autoComplete: PropTypes.string
}

Input.defaultProps = {
	className: "",
	onClick: () => {},
	onChange: () => {},
	autoComplete: "off"
}

export default Input