import React from 'react'
import styled from 'styled-components'

const InputGroupUI = styled.div`
	.input-control {
		padding: 0 .5rem;
	}
`


const InputGroup = ({children}) => (
	<InputGroupUI className="input-group">
		{children[0]}
		{
		children.length > 1 && (
			<div className="input-group-append">
				{children[1]}
			</div>
		)
		}
	</InputGroupUI>
)

export default InputGroup