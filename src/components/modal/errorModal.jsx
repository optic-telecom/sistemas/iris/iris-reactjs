import { Modal } from 'antd';

export default function errorModal(message) {
    Modal.error({
        title: 'Error',
        content: message
    })
}