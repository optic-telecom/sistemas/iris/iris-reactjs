import React from "react";
import PropTypes from "prop-types";
import Button from "./../buttons/Button";

export const ModalHeader = ({ children, close }) => (
	<div className="modal-header">
		<h5 className="mb-sm-0">{children}</h5>
		<Button className="close" onClick={close}>
			x
		</Button>
	</div>
);

export const ModalBody = ({ children }) => (
	<div className="modal-body">{children}</div>
);

export const ModalFooter = ({ children, close }) => (
	<div className="modal-footer">
		<Button color="black" onClick={close}>
			Cerrar
		</Button>
		{children}
	</div>
);

export const Modal = ({ children, isOpen, width, scrollable }) => (
	<>
		{isOpen ? (
			<>
				<div className={`modal fade show`} style={{ display: "block" }}>
					<div
						className="modal-dialog"
						style={{ maxWidth: width > 0 ? `${width}px` : "auto" }}
					>
						<div
							className={`modal-content ${
								scrollable ? "modal--scrollable" : ""
							}`}
						>
							{children}
						</div>
					</div>
				</div>
				<div className={`modal-backdrop fade show`}></div>
			</>
		) : null}
	</>
);

Modal.propTypes = {
	children: PropTypes.node,
	isOpen: PropTypes.bool,
	width: PropTypes.number,
	scrollable: PropTypes.bool,
};

Modal.defaultProps = {
	children: "",
	isOpen: false,
	scrollable: false,
	width: 0,
};
